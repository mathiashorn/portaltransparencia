﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalTransparencia.Models;
using PortalTransparencia.Models.Chart;
using PortalTransparencia.Models.Filter;

namespace PortalTransparencia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConveniosController : ControllerBase
    {
        private readonly PortalTransparenciaContext _context;

        public ConveniosController(PortalTransparenciaContext context)
        {
            _context = context;
        }

        // GET: api/Convenios
        /// <summary>
        /// Listar Convênios (com paginação)
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PagedCollectionResponse<Convenio>>> GetConvenio([FromQuery] ConvenioFilterModel filter)
        {
            //Filtering logic
            int total = 0;
            Func<ConvenioFilterModel, IEnumerable<Convenio>> filterData = (filterModel) =>
            {
                var list = FilterConvenios(filterModel);
                total = list.Count();
                return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Convenio>();
            result.Items = filterData(filter);

            //Get next page URL string  
            ConvenioFilterModel nextFilter = filter.Clone() as ConvenioFilterModel;
            nextFilter.Page += 1;
            String nextUrl = filterData(nextFilter).Count() <= 0 ? null : this.Url.Action("Get", null, nextFilter, Request.Scheme);

            //Get previous page URL string  
            ConvenioFilterModel previousFilter = filter.Clone() as ConvenioFilterModel;
            previousFilter.Page -= 1;
            String previousUrl = previousFilter.Page <= 0 ? null : this.Url.Action("Get", null, previousFilter, Request.Scheme);

            result.NextPage = !String.IsNullOrWhiteSpace(nextUrl) ? new Uri(nextUrl) : null;
            result.PreviousPage = !String.IsNullOrWhiteSpace(previousUrl) ? new Uri(previousUrl) : null;
            result.Total = total;

            return result;

        }

        // GET: api/Convenios/csv
        [Route("csv")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<FileResult> GetConvenioCSV([FromQuery] ConvenioFilterModel filter)
        {

            int total = 0;
            Func<ConvenioFilterModel, IEnumerable<Convenio>> filterData = (filterModel) =>
            {
                var list = FilterConvenios(filterModel);
                total = list.Count();
                //return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
                return list;
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Convenio>();
            result.Items = filterData(filter);

            string fileName = "arquivo.csv";
            byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(ListToCsvData(result.Items.ToList()));

            return File(fileBytes, "text/csv", fileName);

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public static string ListToCsvData(List<Convenio> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list", "Value can not be null or Nothing!");
            }

            StringBuilder sb = new StringBuilder();

            // header
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].Name);

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");
            }

            // content
            foreach (var item in list)
            {

                Type t = item.GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].GetValue(item, null));

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");

            }

            return sb.ToString();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public IQueryable<Convenio> FilterConvenios(ConvenioFilterModel filterModel)
        {
            var result = _context.Convenio.AsQueryable();
            if (filterModel != null)
            {
                if (!string.IsNullOrEmpty(filterModel.Convenio1))
                    result = result.Where(x => x.Convenio1 == filterModel.Convenio1);
                if (filterModel.DataAssinaturaInicial.HasValue)
                    result = result.Where(x => x.DataAssinatura >= filterModel.DataAssinaturaInicial);
                if (filterModel.DataAssinaturaFinal.HasValue)
                    result = result.Where(x => x.DataAssinatura <= filterModel.DataAssinaturaFinal);
                if (!string.IsNullOrEmpty(filterModel.Convenente))
                    result = result.Where(x => x.Convenente.Contains(filterModel.Convenente));
                if (filterModel.ValorRecursoInicial.HasValue && filterModel.ValorRecursoInicial > 0)
                    result = result.Where(x => x.ValorRecurso >= filterModel.ValorRecursoInicial);
                if (filterModel.ValorRecursoFinal.HasValue && filterModel.ValorRecursoFinal > 0)
                    result = result.Where(x => x.ValorRecurso <= filterModel.ValorRecursoFinal);
            }
            return result;
        }

        // GET: api/Convenios/5
        /// <summary>
        /// Listar Convênio por ID
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Convenio>> GetConvenio(int id)
        {
            var convenio = await _context.Convenio.FindAsync(id);

            if (convenio == null)
            {
                return NotFound();
            }

            return convenio;
        }

        // PUT: api/Convenios/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConvenio(int id, Convenio convenio)
        {
            if (id != convenio.Id)
            {
                return BadRequest();
            }

            _context.Entry(convenio).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConvenioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        //// POST: api/Convenios
        //[HttpPost]
        //public async Task<ActionResult<Convenio>> PostConvenio(Convenio convenio)
        //{
        //    _context.Convenio.Add(convenio);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetConvenio", new { id = convenio.Id }, convenio);
        //}

        // POST: api/Convenios
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        public async Task<ActionResult<Convenio>> PostConvenios(List<Convenio> empenhos)
        {
            _context.AddRange(empenhos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConvenio", empenhos);
        }

        // DELETE: api/Convenios/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Convenio>> DeleteConvenio(int id)
        {
            var convenio = await _context.Convenio.FindAsync(id);
            if (convenio == null)
            {
                return NotFound();
            }

            _context.Convenio.Remove(convenio);
            await _context.SaveChangesAsync();

            return convenio;
        }

        // GET: api/Convenios/charts/ano
        [Route("charts/ano")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetConvenioChartsAno()
        {

            var result = _context.Convenio
                            .GroupBy(e => ((DateTime)e.DataAssinatura).Year.ToString())
                            .Select(g => new { orgao = g.Key, count = (decimal)g.Count() })
                            .ToDictionary(k => k.orgao, i => i.count);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        // GET: api/Convenios/charts/tipo
        [Route("charts/tipo")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetConvenioChartsTipo()
        {

            var result = _context.Convenio
                            .GroupBy(e => e.Tipo)
                            .Select(g => new { key = g.Key, value = g.Sum(e => (decimal) e.ValorTotal) })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        // GET: api/Convenios/charts/classificacao
        [Route("charts/classificacao")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetConvenioChartsClassificacao()
        {

            var result = _context.Convenio
                            .GroupBy(e => e.Classificacao)
                            .Select(g => new { key = g.Key, value = g.Sum(e => (decimal)e.ValorTotal) })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        private bool ConvenioExists(int id)
        {
            return _context.Convenio.Any(e => e.Id == id);
        }
    }
}
