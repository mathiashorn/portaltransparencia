﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalTransparencia.Models;
using PortalTransparencia.Models.Chart;
using PortalTransparencia.Models.Filter;

namespace PortalTransparencia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LicitacoesController : ControllerBase
    {
        private readonly PortalTransparenciaContext _context;

        public LicitacoesController(PortalTransparenciaContext context)
        {
            _context = context;
        }

        // GET: api/Licitacoes
        /// <summary>
        /// Listar Licitações (com paginação)
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PagedCollectionResponse<Licitacao>>> GetLicitacao([FromQuery] LicitacaoFilterModel filter)
        {
            //return await _context.Licitacao.ToListAsync();

            //Filtering logic
            int total = 0;
            Func<LicitacaoFilterModel, IEnumerable<Licitacao>> filterData = (filterModel) =>
            {
                var list = FilterLicitacoes(filterModel);
                total = list.Count();
                return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Licitacao>();
            result.Items = filterData(filter);

            //Get next page URL string  
            LicitacaoFilterModel nextFilter = filter.Clone() as LicitacaoFilterModel;
            nextFilter.Page += 1;
            String nextUrl = filterData(nextFilter).Count() <= 0 ? null : this.Url.Action("Get", null, nextFilter, Request.Scheme);

            //Get previous page URL string  
            LicitacaoFilterModel previousFilter = filter.Clone() as LicitacaoFilterModel;
            previousFilter.Page -= 1;
            String previousUrl = previousFilter.Page <= 0 ? null : this.Url.Action("Get", null, previousFilter, Request.Scheme);

            result.NextPage = !String.IsNullOrWhiteSpace(nextUrl) ? new Uri(nextUrl) : null;
            result.PreviousPage = !String.IsNullOrWhiteSpace(previousUrl) ? new Uri(previousUrl) : null;
            result.Total = total;

            return result;

        }

        // GET: api/Licitacoes/csv
        [Route("csv")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<FileResult> GetLicitacaoCSV([FromQuery] LicitacaoFilterModel filter)
        {

            int total = 0;
            Func<LicitacaoFilterModel, IEnumerable<Licitacao>> filterData = (filterModel) =>
            {
                var list = FilterLicitacoes(filterModel);
                total = list.Count();
                //return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
                return list;
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Licitacao>();
            result.Items = filterData(filter);

            string fileName = "arquivo.csv";
            byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(ListToCsvData(result.Items.ToList()));

            return File(fileBytes, "text/csv", fileName);

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public static string ListToCsvData(List<Licitacao> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list", "Value can not be null or Nothing!");
            }

            StringBuilder sb = new StringBuilder();

            // header
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].Name);

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");
            }

            // content
            foreach (var item in list)
            {

                Type t = item.GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].GetValue(item, null));

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");

            }

            return sb.ToString();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public IQueryable<Licitacao> FilterLicitacoes(LicitacaoFilterModel filterModel)
        {
            var result = _context.Licitacao.AsQueryable();
            if (filterModel != null)
            {
                if (!string.IsNullOrEmpty(filterModel.NrLicitacao))
                    result = result.Where(x => x.NrLicitacao == filterModel.NrLicitacao);
                if (filterModel.AnoLicitacao.HasValue)
                    result = result.Where(x => x.AnoLicitacao == filterModel.AnoLicitacao);
                if (!string.IsNullOrEmpty(filterModel.CdTipoModalidade))
                    result = result.Where(x => x.CdTipoModalidade == filterModel.CdTipoModalidade);
                if (filterModel.VlLicitacaoInicial.HasValue && filterModel.VlLicitacaoInicial > 0)
                    result = result.Where(x => x.VlLicitacao >= filterModel.VlLicitacaoInicial);
                if (filterModel.VlLicitacaoFinal.HasValue && filterModel.VlLicitacaoFinal > 0)
                    result = result.Where(x => x.VlLicitacao <= filterModel.VlLicitacaoFinal);
            }
            return result;
        }

        // GET: api/Licitacoes/5
        /// <summary>
        /// Listar Licitação por ID
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Licitacao>> GetLicitacao(int id)
        {
            var licitacao = await _context.Licitacao.FindAsync(id);

            //empenhos da licitação
            var empenhos = _context.Empenho.Where(e => 
                e.NumeroLicitacao == licitacao.NrLicitacao &&
                e.AnoLicitacao == licitacao.AnoLicitacao &&
                e.ModalidadeLicitacao == licitacao.CdTipoModalidade).ToList();
            licitacao.Empenhos = empenhos;

            if (licitacao == null)
            {
                return NotFound();
            }

            return licitacao;
        }

        // PUT: api/Licitacoes/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLicitacao(int id, Licitacao licitacao)
        {
            if (id != licitacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(licitacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LicitacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        //// POST: api/Licitacoes
        //[Authorize]
        //[HttpPost]
        //public async Task<ActionResult<Licitacao>> PostLicitacao(Licitacao licitacao)
        //{
        //    _context.Licitacao.Add(licitacao);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetLicitacao", new { id = licitacao.Id }, licitacao);
        //}

        // POST: api/Licitacoes
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        public async Task<ActionResult<Licitacao>> PostLicitacoes(List<Licitacao> licitacoes)
        {
            _context.AddRange(licitacoes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLicitacao", licitacoes);
        }

        // DELETE: api/Licitacoes/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Licitacao>> DeleteLicitacao(int id)
        {
            var licitacao = await _context.Licitacao.FindAsync(id);
            if (licitacao == null)
            {
                return NotFound();
            }

            _context.Licitacao.Remove(licitacao);
            await _context.SaveChangesAsync();

            return licitacao;
        }

        // GET: api/Licitacoes/charts/ano
        [Route("charts/ano")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetLicitacaoChartsAno()
        {

            var result = _context.Licitacao
                            .GroupBy(e => e.AnoLicitacao.ToString())
                            .Select(g => new { key = g.Key, value = (decimal)g.Count() })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        // GET: api/Licitacoes/charts/modalidade
        [Route("charts/modalidade")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public ActionResult<ChartObject> GetLicitacaoChartsOrgao()
        {

            var result = _context.Licitacao
                            .GroupBy(e => e.CdTipoModalidade)
                            .Select(g => new { key = g.Key, value = (decimal) g.Count() })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        private bool LicitacaoExists(int id)
        {
            return _context.Licitacao.Any(e => e.Id == id);
        }
    }
}
