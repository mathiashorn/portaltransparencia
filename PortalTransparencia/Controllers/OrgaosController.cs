﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalTransparencia.Models;

namespace PortalTransparencia.Controllers
{
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class OrgaosController : ControllerBase
    {
        private readonly PortalTransparenciaContext _context;

        public OrgaosController(PortalTransparenciaContext context)
        {
            _context = context;
        }

        // GET: api/Orgaos
        /// <summary>
        /// Listar Órgãos
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Orgao>>> GetOrgao()
        {
            return await _context.Orgao.OrderBy(o => o.Nome).ToListAsync();
        }

        // GET: api/Orgaos/5
        /// <summary>
        /// Listar Órgão por ID
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Orgao>> GetOrgao(int id)
        {
            var orgao = await _context.Orgao.FindAsync(id);

            if (orgao == null)
            {
                return NotFound();
            }

            return orgao;
        }

        // PUT: api/Orgaos/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrgao(int id, Orgao orgao)
        {
            if (id != orgao.Id)
            {
                return BadRequest();
            }

            _context.Entry(orgao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrgaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Orgaos
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        public async Task<ActionResult<Orgao>> PostOrgao(Orgao orgao)
        {
            _context.Orgao.Add(orgao);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrgaoExists(orgao.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetOrgao", new { id = orgao.Id }, orgao);
        }

        // DELETE: api/Orgaos/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Orgao>> DeleteOrgao(int id)
        {
            var orgao = await _context.Orgao.FindAsync(id);
            if (orgao == null)
            {
                return NotFound();
            }

            _context.Orgao.Remove(orgao);
            await _context.SaveChangesAsync();

            return orgao;
        }

        private bool OrgaoExists(int id)
        {
            return _context.Orgao.Any(e => e.Id == id);
        }
    }
}
