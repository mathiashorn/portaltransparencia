﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalTransparencia.Models;
using PortalTransparencia.Models.Chart;
using PortalTransparencia.Models.Filter;

namespace PortalTransparencia.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class EmpenhosController : ControllerBase
    {
        private readonly PortalTransparenciaContext _context;

        public EmpenhosController(PortalTransparenciaContext context)
        {
            _context = context;
        }

        // GET: api/Empenhos
        /// <summary>
        /// Listar Empenhos (com paginação)
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PagedCollectionResponse<Empenho>>> GetEmpenho([FromQuery] EmpenhoFilterModel filter)
        {
            //return await _context.Empenho.ToListAsync();

            //Filtering logic
            int total = 0;
            Func<EmpenhoFilterModel, IEnumerable<Empenho>> filterData = (filterModel) =>
            {
                var list = FilterEmpenhos(filterModel);
                total = list.Count();
                return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Empenho>();
            result.Items = filterData(filter);

            //Get next page URL string  
            EmpenhoFilterModel nextFilter = filter.Clone() as EmpenhoFilterModel;
            nextFilter.Page += 1;
            String nextUrl = filterData(nextFilter).Count() <= 0 ? null : this.Url.Action("Get", null, nextFilter, Request.Scheme);

            //Get previous page URL string  
            EmpenhoFilterModel previousFilter = filter.Clone() as EmpenhoFilterModel;
            previousFilter.Page -= 1;
            String previousUrl = previousFilter.Page <= 0 ? null : this.Url.Action("Get", null, previousFilter, Request.Scheme);

            result.NextPage = !String.IsNullOrWhiteSpace(nextUrl) ? new Uri(nextUrl) : null;
            result.PreviousPage = !String.IsNullOrWhiteSpace(previousUrl) ? new Uri(previousUrl) : null;
            result.Total = total;

            return result;

        }

        // GET: api/Empenhos/csv
        [Route("csv")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<FileResult> GetEmpenhoCSV([FromQuery] EmpenhoFilterModel filter)
        {

            int total = 0;
            Func<EmpenhoFilterModel, IEnumerable<Empenho>> filterData = (filterModel) =>
            {
                var list = FilterEmpenhos(filterModel);
                total = list.Count();
                //return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
                return list;
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Empenho>();
            result.Items = filterData(filter);

            string fileName = "arquivo.csv";
            byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(ListToCsvData(result.Items.ToList()));

            return File(fileBytes, "text/csv", fileName);

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public static string ListToCsvData(List<Empenho> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list", "Value can not be null or Nothing!");
            }

            StringBuilder sb = new StringBuilder();

            // header
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].Name);

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");
            }

            // content
            foreach (var item in list)
            {

                Type t = item.GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].GetValue(item, null));

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");

            }

            return sb.ToString();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public IQueryable<Empenho> FilterEmpenhos(EmpenhoFilterModel filterModel)
        {
            var result = _context.Empenho.AsQueryable();
            if (filterModel != null)
            {
                if (!string.IsNullOrEmpty(filterModel.NumeroEmpenho))
                    result = result.Where(x => x.NumeroEmpenho == filterModel.NumeroEmpenho);
                if (filterModel.DataEmpenhoInicial.HasValue)
                    result = result.Where(x => x.DataEmpenho >= filterModel.DataEmpenhoInicial);
                if (filterModel.DataEmpenhoFinal.HasValue)
                    result = result.Where(x => x.DataEmpenho <= filterModel.DataEmpenhoFinal);
                if (filterModel.CodigoOrgao.HasValue)
                    result = result.Where(x => x.OrgaoId == filterModel.CodigoOrgao);
                if (filterModel.ValorEmpenhoInicial.HasValue && filterModel.ValorEmpenhoInicial > 0)
                    result = result.Where(x => x.ValorEmpenho >= filterModel.ValorEmpenhoInicial);
                if (filterModel.ValorEmpenhoFinal.HasValue && filterModel.ValorEmpenhoFinal > 0)
                    result = result.Where(x => x.ValorEmpenho <= filterModel.ValorEmpenhoFinal);
            }
            return result;
        }

        // GET: api/Empenhos/5
        /// <summary>
        /// Listar Empenho por ID
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Empenho>> GetEmpenho(int id)
        {
            var empenho = await _context.Empenho.FindAsync(id);

            if (empenho == null)
            {
                return NotFound();
            }

            return empenho;
        }

        // PUT: api/Empenhos/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpenho(int id, Empenho empenho)
        {
            if (id != empenho.Id)
            {
                return BadRequest();
            }

            _context.Entry(empenho).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpenhoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        //// POST: api/Empenhos
        //[HttpPost]
        //public async Task<ActionResult<Empenho>> PostEmpenho(Empenho empenho)
        //{
        //    _context.Empenho.Add(empenho);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetEmpenho", new { id = empenho.Id }, empenho);
        //}

        // POST: api/Empenhos
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        public async Task<ActionResult<Empenho>> PostEmpenhos(List<Empenho> empenhos)
        {
            _context.AddRange(empenhos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEmpenho", empenhos);
        }

        // DELETE: api/Empenhos/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Empenho>> DeleteEmpenho(int id)
        {
            var empenho = await _context.Empenho.FindAsync(id);
            if (empenho == null)
            {
                return NotFound();
            }

            _context.Empenho.Remove(empenho);
            await _context.SaveChangesAsync();

            return empenho;
        }

        // GET: api/Empenhos/charts/ano
        [Route("charts/ano")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetEmpenhoChartsAno()
        {

            var result = _context.Empenho
                            .GroupBy(e => ((DateTime)e.DataEmpenho).Year.ToString())
                            .Select(g => new { key = g.Key, value = (decimal)g.Count() })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        // GET: api/Empenhos/charts/orgao
        [Route("charts/orgao")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetEmpenhoChartsOrgao()
        {

            var result = _context.Empenho
                            .GroupBy(e => e.Orgao.Nome)
                            .Select(g => new { key = g.Key, value = g.Sum(e => (decimal) e.ValorEmpenho) })
                            .ToDictionary(k => k.key, i => i.value);

                ChartObject chartObject = new ChartObject();
                chartObject.Options = result.Keys.ToList();
                chartObject.Series = result.Values.ToList();

                return chartObject;
            
        }

        private bool EmpenhoExists(int id)
        {
            return _context.Empenho.Any(e => e.Id == id);
        }
    }
}
