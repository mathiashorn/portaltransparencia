﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PortalTransparencia.Models;
using PortalTransparencia.Models.Chart;
using PortalTransparencia.Models.Filter;

namespace PortalTransparencia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServidoresController : ControllerBase
    {
        private readonly PortalTransparenciaContext _context;

        public ServidoresController(PortalTransparenciaContext context)
        {
            _context = context;
        }

        // GET: api/Servidores
        /// <summary>
        /// Listar Servidores (com paginação)
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PagedCollectionResponse<Servidor>>> GetServidor([FromQuery] ServidorFilterModel filter)
        {
            //return await _context.Servidor.ToListAsync();

            //Filtering logic
            int total = 0;
            Func<ServidorFilterModel, IEnumerable<Servidor>> filterData = (filterModel) =>
            {
                var list = FilterServidores(filterModel);
                total = list.Count();
                return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Servidor>();
            result.Items = filterData(filter);

            //Get next page URL string  
            ServidorFilterModel nextFilter = filter.Clone() as ServidorFilterModel;
            nextFilter.Page += 1;
            String nextUrl = filterData(nextFilter).Count() <= 0 ? null : this.Url.Action("Get", null, nextFilter, Request.Scheme);

            //Get previous page URL string  
            ServidorFilterModel previousFilter = filter.Clone() as ServidorFilterModel;
            previousFilter.Page -= 1;
            String previousUrl = previousFilter.Page <= 0 ? null : this.Url.Action("Get", null, previousFilter, Request.Scheme);

            result.NextPage = !String.IsNullOrWhiteSpace(nextUrl) ? new Uri(nextUrl) : null;
            result.PreviousPage = !String.IsNullOrWhiteSpace(previousUrl) ? new Uri(previousUrl) : null;
            result.Total = total;

            return result;

        }

        // GET: api/Servidores/csv
        [Route("csv")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<FileResult> GetServidorCSV([FromQuery] ServidorFilterModel filter)
        {

            int total = 0;
            Func<ServidorFilterModel, IEnumerable<Servidor>> filterData = (filterModel) =>
            {
                var list = FilterServidores(filterModel);
                total = list.Count();
                //return list.Skip(filterModel.Page * filter.Limit).Take(filterModel.Limit);
                return list;
            };

            //Get the data for the current page  
            var result = new PagedCollectionResponse<Servidor>();
            result.Items = filterData(filter);

            string fileName = "arquivo.csv";
            byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(ListToCsvData(result.Items.ToList()));

            return File(fileBytes, "text/csv", fileName);

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public static string ListToCsvData(List<Servidor> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list", "Value can not be null or Nothing!");
            }

            StringBuilder sb = new StringBuilder();

            // header
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].Name);

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");
            }

            // content
            foreach (var item in list)
            {

                Type t = item.GetType();
                PropertyInfo[] pi = t.GetProperties();

                for (int index = 0; index < pi.Length; index++)
                {
                    sb.Append(pi[index].GetValue(item, null));

                    if (index < pi.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("\r\n");

            }

            return sb.ToString();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public IQueryable<Servidor> FilterServidores(ServidorFilterModel filterModel)
        {
            var result = _context.Servidor.AsQueryable();
            if (filterModel != null)
            {
                if (!string.IsNullOrEmpty(filterModel.Nome))
                    result = result.Where(x => x.Nome.Contains(filterModel.Nome));
                if (!string.IsNullOrEmpty(filterModel.NomeCargo))
                    result = result.Where(x => x.NomeCargo.Contains(filterModel.NomeCargo));
                if (filterModel.ValorRemuneracaoInicial.HasValue && filterModel.ValorRemuneracaoInicial > 0)
                    result = result.Where(x => (x.ValorRemuneracaoBasica - x.ValorDeducoesObrigatorias) >= filterModel.ValorRemuneracaoInicial);
                if (filterModel.ValorRemuneracaoFinal.HasValue && filterModel.ValorRemuneracaoFinal > 0)
                    result = result.Where(x => (x.ValorRemuneracaoBasica - -x.ValorDeducoesObrigatorias) <= filterModel.ValorRemuneracaoFinal);
            }
            return result;
        }

        // GET: api/Servidores/5
        /// <summary>
        /// Listar Servidor por ID
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Servidor>> GetServidor(int id)
        {
            var servidor = await _context.Servidor.FindAsync(id);

            if (servidor == null)
            {
                return NotFound();
            }

            return servidor;
        }

        // PUT: api/Servidores/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutServidor(int id, Servidor servidor)
        {
            if (id != servidor.Id)
            {
                return BadRequest();
            }

            _context.Entry(servidor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServidorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        //// POST: api/Servidores
        //[HttpPost]
        //public async Task<ActionResult<Servidor>> PostServidor(Servidor servidor)
        //{
        //    _context.Servidor.Add(servidor);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetServidor", new { id = servidor.Id }, servidor);
        //}

        // POST: api/Empenhos
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        public async Task<ActionResult<Servidor>> PostServidores(List<Servidor> servidores)
        {
            try
            {
                _context.AddRange(servidores);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw;
            }

            return CreatedAtAction("GetServidor", servidores);
        }


        // DELETE: api/Servidores/5
        [Authorize]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Servidor>> DeleteServidor(int id)
        {
            var servidor = await _context.Servidor.FindAsync(id);
            if (servidor == null)
            {
                return NotFound();
            }

            _context.Servidor.Remove(servidor);
            await _context.SaveChangesAsync();

            return servidor;
        }

        // GET: api/Servidores/charts/cargo
        [Route("charts/cargo")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetServidoresChartsCargo()
        {

            var result = _context.Servidor
                            .GroupBy(e => e.NomeCargo)
                            .Select(g => new { key = g.Key, value = (decimal) g.Count() })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        // GET: api/Servidores/charts/orgao
        [Route("charts/orgao")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetServidoresChartsOrgao()
        {

            var result = _context.Servidor
                            .GroupBy(e => e.NomeOrgao)
                            .Select(g => new { key = g.Key, value = (decimal) g.Count() })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        // GET: api/Servidores/charts/orgao
        [Route("charts/orgao/valor")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public async Task<ActionResult<ChartObject>> GetServidoresChartsOrgaoValor()
        {

            var result = _context.Servidor
                            .GroupBy(e => e.NomeOrgao)
                            .Select(g => new { key = g.Key, value = g.Sum(e => (decimal) (e.ValorRemuneracaoBasica - e.ValorDeducoesObrigatorias )) })
                            .ToDictionary(k => k.key, i => i.value);

            ChartObject chartObject = new ChartObject();
            chartObject.Options = result.Keys.ToList();
            chartObject.Series = result.Values.ToList();

            return chartObject;

        }

        private bool ServidorExists(int id)
        {
            return _context.Servidor.Any(e => e.Id == id);
        }
    }
}
