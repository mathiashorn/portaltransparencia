﻿using PortalTransparencia.Models;
using System;

namespace PortalTransparencia.Services
{
    public interface IAuthenticateService
    {
        bool IsAuthenticated(TokenRequest request, out string token, out DateTime? expires);
    }
}
