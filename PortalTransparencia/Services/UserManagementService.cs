﻿using PortalTransparencia.Models;
using System.Linq;

namespace PortalTransparencia.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly PortalTransparenciaContext _context;

        public UserManagementService(PortalTransparenciaContext context)
        {
            _context = context;
        }

        public bool IsValidUser(string userName, string password)
        {
            var user = _context.Usuario
                .FirstOrDefault(u => u.Email == userName
                             && u.Senha == password);

            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
