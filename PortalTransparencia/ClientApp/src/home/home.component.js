﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import AppBar from '../_components/appbar';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';

const drawerWidth = 0;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    }
});
class Home extends Component {
    render() {
        const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    {/*<Nav />*/}
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <div className={classes.heroContent}>
                            <Container maxWidth="sm">
                                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                                    Portal da Transparência
                                </Typography>
                                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                                    Município de Lajeado
                                </Typography>
                                <div className={classes.heroButtons}>
                                    <Grid container spacing={2} justify="center">
                                        <Grid item>
                                            <Link href="/home" color="inherit" className={classes.link} style={{ textDecoration: 'none' }}>
                                                <Button variant="outlined" color="primary">
                                                    Área do Cidadão
                                                </Button>
                                            </Link>
                                        </Grid>
                                        <Grid item>
                                            <Link href="/desenvolvedores" color="inherit" className={classes.link} style={{ textDecoration: 'none' }} target="blank" >
                                                <Button variant="contained" color="primary">
                                                    Área do Desenvolvedor
                                                </Button>
                                            </Link>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Container>
                        </div>
                        <Container className={classes.cardGrid} maxWidth="md">
                            {/* End hero unit */}
                            <Grid container spacing={4}>
                                <Grid item key="convenios" xs={12} sm={6} md={4}>
                                    <Card className={classes.card}>
                                        <CardMedia
                                            className={classes.cardMedia}
                                            image={require('../img/convenios.png')}
                                            title="Image title"
                                        />
                                        <CardContent className={classes.cardContent}>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                Convênios
                                            </Typography>
                                            <Typography>
                                                Informações sobre os convênios
                                            </Typography>
                                        </CardContent>
                                        <CardActions align="center">
                                            <Button size="small" color="primary" href="/convenios">
                                                Ver Dados
                                            </Button>
                                            <Button size="small" color="primary" href="/convenios/graficos">
                                                Ver Gráficos
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                                <Grid item key="empenho" xs={12} sm={6} md={4}>
                                    <Card className={classes.card}>
                                        <CardMedia
                                            className={classes.cardMedia}
                                            image={require('../img/empenhos.png')}
                                            title="Image title"
                                        />
                                        <CardContent className={classes.cardContent}>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                Empenhos
                                            </Typography>
                                            <Typography>
                                                Informações sobre os empenhos
                                            </Typography>
                                        </CardContent>
                                        <CardActions align="center">
                                            <Button size="small" color="primary" href="/empenhos">
                                                Ver Dados
                                            </Button>
                                            <Button size="small" color="primary" href="/empenhos/graficos">
                                                Ver Gráficos
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                                <Grid item key="licitacao" xs={12} sm={6} md={4}>
                                    <Card className={classes.card}>
                                        <CardMedia
                                            className={classes.cardMedia}
                                            image={require('../img/licitacoes.png')}
                                            title="Image title"
                                        />
                                        <CardContent className={classes.cardContent}>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                Licitações
                                                </Typography>
                                            <Typography>
                                                Informações sobre as licitações
                                                </Typography>
                                        </CardContent>
                                        <CardActions align="center">
                                            <Button size="small" color="primary" href="/licitacoes">
                                                Ver Dados
                                            </Button>
                                            <Button size="small" color="primary" href="/licitacoes/graficos">
                                                Ver Gráficos
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid> 
                                <Grid item key="servidores" xs={12} sm={6} md={4}>
                                    <Card className={classes.card}>
                                        <CardMedia
                                            className={classes.cardMedia}
                                            image={require('../img/servidores.png')}
                                            title="Image title"
                                        />
                                        <CardContent className={classes.cardContent}>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                Servidores
                                                </Typography>
                                            <Typography>
                                                Informações sobre os servidores públicos e suas remunerações
                                                </Typography>
                                        </CardContent>
                                        <CardActions align="center">
                                            <Button size="small" color="primary" href="/servidores">
                                                Ver Dados
                                            </Button>
                                            <Button size="small" color="primary" href="/servidores/graficos">
                                                Ver Gráficos
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>                                                                   
                            </Grid>
                        </Container>
                    </main>
                </div>
            </div>
        );
    }
}
Home.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    return state;
}
const connectedHomePage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Home)));
export { connectedHomePage as Home };