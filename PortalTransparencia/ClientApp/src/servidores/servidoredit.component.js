﻿import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { servidorAction } from '../_actions';
import { withRouter } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from '@material-ui/core/FormControl';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const drawerWidth = 0;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
});
class ServidorEdit extends Component {

    handleChange = prop => event => {
        const { dispatch } = this.props;
        dispatch(servidorAction.onChangeProps(prop, event));
    };

    componentDidMount() {
        const { match: { params } } = this.props;
        if (params.id) {            
            const { dispatch } = this.props;
            dispatch(servidorAction.getServidorById(params.id));
        }
    }

    render() {
        const { classes } = this.props;
        const { match: { params } } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    {/*<Nav />*/}
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        {/*<Grid container spacing={2}>
                            <Grid item xs={3}>
                                <Typography>{'Visualizar Servidor'}</Typography>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">
                            </Grid>
                        </Grid>
                        <br /><br />*/}
                        <Grid container spacing={10}>
                            <Grid item xs={12}>
                                <div>
                                    <Paper className={classes.contentRoot} elevation={1}>
                                        <Breadcrumbs aria-label="breadcrumb">
                                            <Link color="inherit" href="/home" >
                                                Área do Cidadão
                                        </Link>
                                            <Link color="inherit" href="/servidores/" >
                                                Servidores
                                        </Link>
                                            <Typography color="textPrimary">Detalhes do Servidor</Typography>
                                        </Breadcrumbs>
                                        <br />
                                        <form className={classes.container}>
                                            <Grid container spacing={1}>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="matricula"
                                                            label="Matrícula"
                                                            className={classes.textField}
                                                            value={this.props.servidor.matricula}
                                                            onChange={this.handleChange('matricula')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid> 
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="nome"
                                                            label="Nome"
                                                            className={classes.textField}
                                                            value={this.props.servidor.nome}
                                                            onChange={this.handleChange('nome')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="horasMensais"
                                                            label="Horas Mensais"
                                                            className={classes.textField}
                                                            value={this.props.servidor.horasMensais}
                                                            onChange={this.handleChange('horasMensais')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="descTipoMov"
                                                            label="Situação"
                                                            className={classes.textField}
                                                            value={this.props.servidor.descTipoMov}
                                                            onChange={this.handleChange('descTipoMov')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataAdmissao"
                                                            label="Data Admissão"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            value={this.props.servidor.dataAdmissao}
                                                            onChange={this.handleChange('dataAdmissao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="nomeOrgao"
                                                            label="Órgão"
                                                            className={classes.textField}
                                                            multiline
                                                            value={this.props.servidor.nomeOrgao}
                                                            onChange={this.handleChange('nomeOrgao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="nomeCargo"
                                                            label="Cargo"
                                                            className={classes.textField}
                                                            multiline
                                                            value={this.props.servidor.nomeCargo}
                                                            onChange={this.handleChange('nomeCargo')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorRemuneracaoBasica"
                                                            label="Valor Base (+)"
                                                            className={classes.textField}
                                                            value={this.props.servidor.valorRemuneracaoBasica}
                                                            onChange={this.handleChange('valorRemuneracaoBasica')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                {/*<Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorVerbasEventuais"
                                                            label="Eventuais (+)"
                                                            className={classes.textField}
                                                            value={this.props.servidor.valorVerbasEventuais}
                                                            onChange={this.handleChange('valorVerbasEventuais')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorVerbasIndenizatorias"
                                                            label="Indenizatórias (+)"
                                                            className={classes.textField}
                                                            value={this.props.servidor.valorVerbasIndenizatorias}
                                                            onChange={this.handleChange('valorVerbasIndenizatorias')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={1}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorFerias"
                                                            label="Férias (+)"
                                                            className={classes.textField}
                                                            value={this.props.servidor.valorFerias}
                                                            onChange={this.handleChange('valorFerias')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={1}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorDecimoTerceiro"
                                                            label="13º (+)"
                                                            className={classes.textField}
                                                            value={this.props.servidor.valorDecimoTerceiro}
                                                            onChange={this.handleChange('valorDecimoTerceiro')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>*/}
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorDeducoesObrigatorias"
                                                            label="Deduções Obrig. (-)"
                                                            className={classes.textField}
                                                            value={this.props.servidor.valorDeducoesObrigatorias}
                                                            onChange={this.handleChange('valorDeducoesObrigatorias')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorRemuneracao"
                                                            label="Valor Final (=)"
                                                            className={classes.textField}
                                                            value={(this.props.servidor.valorRemuneracaoBasica - this.props.servidor.valorDeducoesObrigatorias).toFixed(2)}
                                                            onChange={this.handleChange('valorRemuneracao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <br />
                                            <Grid container spacing={10}>
                                                <Grid item xs={12} container justify="flex-end">
                                                    <Grid item xs={6} container justify="flex-end">
                                                        <Button variant="contained" color="secondary" className={classes.button} component='a' href="/servidores">Voltar</Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </form>
                                    </Paper>
                                </div>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
ServidorEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return state;
}

const connectedServidorEditPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(ServidorEdit)));
export { connectedServidorEditPage as ServidorEdit };