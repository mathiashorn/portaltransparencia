﻿import { connect } from 'react-redux';
import { servidorAction } from '../_actions';
import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import ReactTable from 'react-table';
import Select from '@material-ui/core/Select';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Moment from 'react-moment';
import 'moment-timezone';

const drawerWidth = 0;
const styles = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    paper: {
        padding: theme.spacing(3),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    }
});
class Servidor extends Component {

    state = {
        filterModel: {
            page: 0,
            limit: 10,
            nome: '',
            nomeCargo: '',
            valorRemuneracaoInicial: 0,
            valorRemuneracaoFinal: 0
        }
    };

    handleChangePageProps(value) {
        var val = value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.page = val;
            return { filterModel };
        });
        this.forceUpdate();
        this.refreshData(this.state.filterModel);
    }

    handleChangeLimitProps(value) {
        var val = value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.limit = val;
            filterModel.page = 0;
            return { filterModel };
        });
        this.forceUpdate();
        this.refreshData(this.state.filterModel);
    }

    componentDidMount() {
        const { dispatch } = this.props;
        this.refreshData(this.state.filterModel);
    }
    refreshData = (filterModel) => {
        const { dispatch } = this.props;
        dispatch(servidorAction.getServidorPagination(filterModel));
    }

    handleChangeNome = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.nome = val;
            return { filterModel };
        });
    }
    handleChangeNomeCargo = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.nomeCargo = val;
            return { filterModel };
        });
    }
    handleChangeValorRemuneracaoInicial = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.valorRemuneracaoInicial = val;
            return { filterModel };
        });
    }
    handleChangeValorRemuneracaoFinal = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.valorRemuneracaoFinal = val;
            return { filterModel };
        });
    }
    handleClick = (event, id) => {
        this.state.filterModel.page = 0;
        this.state.filterModel.limit = 10;
        this.refreshData(this.state.filterModel);
    };
    handleExport = (event, id) => {
        const { dispatch } = this.props;
        dispatch(servidorAction.getServidorCSV(this.state.filterModel));
    };

    render() {

        const { classes } = this.props;
        const { servidor } = this.props.state.servidor;
        const { totalItems } = this.props.state.servidor;
        let { limit, page } = this.state.filterModel;

        const handleChangePage = (event, newPage) => {
            this.handleChangePageProps(newPage);
        };

        const handleChangeRowsPerPage = event => {
            this.handleChangeLimitProps(parseInt(event.target.value, 10));
        };

        return (
            <div>
                <div>
                    <AppBar />
                    <main className={classes.content}>
                        <div className={classes.appBarSpacer} />
                        <Container maxWidth="lg" className={classes.container}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>    
                                        <Grid container spacing={2}>
                                            <Grid item xs={6}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="nome"
                                                        label="Nome do Servidor"
                                                        className={classes.textField}
                                                        value={this.state.filterModel.nome}
                                                        onChange={this.handleChangeNome('nome')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="nomeCargo"
                                                        label="Cargo do Servidor"
                                                        className={classes.textField}
                                                        value={this.state.filterModel.nomeCargo}
                                                        onChange={this.handleChangeNomeCargo('nomeCargo')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="valorRemuneracaoInicial"
                                                        label="Valor Remuneração Inicial"
                                                        className={classes.textField}
                                                        type="number"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeValorRemuneracaoInicial('valorRemuneracaoInicial')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="valorRemuneracaoFinal"
                                                        label="Valor Remuneração Final"
                                                        className={classes.textField}
                                                        type="number"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeValorRemuneracaoFinal('valorRemuneracaoFinal')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                        </Grid>
                                        <br />
                                        <Grid container spacing={24}>
                                            <Grid item xs={3}>
                                            </Grid>
                                            <Grid item xs={6}>
                                            </Grid>
                                            <Grid item xs={3} container justify="center">
                                                <Grid container spacing={24}>
                                                    <Grid item xs={12} container justify="flex-end">
                                                        <Button variant="contained" color="default" className={classes.button} onClick={(event) => this.handleExport(event)} >Exportar</Button>
                                                        <Button variant="contained" color="primary" className={classes.button} onClick={(event) => this.handleClick(event)}>Pesquisar</Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell>Nome Servidor</TableCell>
                                                    <TableCell>Cargo do Servidor</TableCell>
                                                    <TableCell>Remuneração</TableCell>
                                                    <TableCell>Ações</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    servidor.map(n => {
                                                        return (
                                                            <TableRow key={n.id}>
                                                                <TableCell component="th" scope="row">
                                                                    {n.nome}
                                                                </TableCell>
                                                                <TableCell>
                                                                    {n.nomeCargo}
                                                                </TableCell>
                                                                <TableCell>
                                                                    {(n.valorRemuneracaoBasica - n.valorDeducoesObrigatorias).toFixed(2)}
                                                                </TableCell>
                                                                <TableCell>
                                                                    <IconButton className={classes.button} aria-label="Edit" component='a' href={`/servidor/${n.id}`} target="_blank">
                                                                        <OpenInNewIcon />
                                                                    </IconButton>
                                                                </TableCell>
                                                            </TableRow>
                                                        );
                                                    })
                                                }
                                            </TableBody>
                                            <TableFooter>
                                                <TableRow>
                                                    <TablePagination
                                                        rowsPerPageOptions={[5, 10, 25]}
                                                        colSpan={4}
                                                        count={totalItems}
                                                        rowsPerPage={limit}
                                                        page={page}
                                                        SelectProps={{
                                                            inputProps: { 'aria-label': 'Registros por página' },
                                                            native: true,
                                                        }}
                                                        onChangePage={handleChangePage}
                                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                                        labelRowsPerPage={'Registros por página'}
                                                    />
                                                </TableRow>
                                            </TableFooter>
                                        </Table>
                                    </Paper>
                                </Grid>
                            </Grid>
                        </Container>
                    </main>
                </div>
            </div>
        );
    }
}
Servidor.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return {
        state
    };
}
const connectedServidorPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Servidor)));
export { connectedServidorPage as Servidor };