﻿import { connect } from 'react-redux';
import React, { Component } from 'react';
import AppBar from '../api/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Nav from '../api/nav';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { withRouter } from 'react-router-dom';
import Divider from '@material-ui/core/Divider';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
];

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100vh',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
});
class Sobre extends Component {

    componentDidMount() {
    }
    render() {

        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    <Nav />
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <Grid container spacing={24}>
                            <Grid item xs={12} justify="flex-start" >
                                <Typography variant="h3">{'Sobre'}</Typography>
                                <Divider />
                            </Grid>
                        </Grid>
                        <br />
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <Typography variant="body1" gutterBottom>
                                    A API do Portal da Transparência está baseada em uma arquitetura REST, logo retorna códigos de resposta HTTP, além do JSON de retorno das requisições.
                                </Typography>
                            </Grid>
                            <br />
                            <br />
                            <Grid item xs={12} justify="flex-start">
                                <Typography variant="h4">{'Códigos de Retorno HTTP'}</Typography>
                                <Divider />
                            </Grid>
                            <br />
                            <br />
                            <Grid container spacing={24}>
                                <Paper className={classes.root}>
                                    <Table className={classes.table}>
                                        <TableHead>
                                            <TableRow>
                                                <StyledTableCell>Dessert (100g serving)</StyledTableCell>
                                                <StyledTableCell align="right">Calories</StyledTableCell>
                                                <StyledTableCell align="right">Fat&nbsp;(g)</StyledTableCell>
                                                <StyledTableCell align="right">Carbs&nbsp;(g)</StyledTableCell>
                                                <StyledTableCell align="right">Protein&nbsp;(g)</StyledTableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {rows.map(row => (
                                                <StyledTableRow key={row.name}>
                                                    <StyledTableCell component="th" scope="row">
                                                        {row.name}
                                                    </StyledTableCell>
                                                    <StyledTableCell align="right">{row.calories}</StyledTableCell>
                                                    <StyledTableCell align="right">{row.fat}</StyledTableCell>
                                                    <StyledTableCell align="right">{row.carbs}</StyledTableCell>
                                                    <StyledTableCell align="right">{row.protein}</StyledTableCell>
                                                </StyledTableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </Paper>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
Sobre.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return {
        state
    };
}
const connectedSobrePage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Sobre)));
export { connectedSobrePage as Sobre };