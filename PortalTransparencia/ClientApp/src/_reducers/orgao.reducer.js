﻿const initialState = {
    anchor: 'left',
    orgao: [],
    open: false,
    id: '',
    nome: ''
};

export function orgao(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_ORGAO':
            return {
                ...state,
                orgao: action.orgao
            };
        case 'ORGAO_DETAIL':
            return {
                ...state,
                id: action.id,
                nome: action.nome
            };
        case "ORGAO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
