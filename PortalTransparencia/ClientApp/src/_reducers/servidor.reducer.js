﻿const initialState = {
    anchor: 'left',
    servidor: [],
    open: false,
    page: 1,
    limit: 5,
    id: '',
    matricula: '',
    nome: '',
    codigoCargo: 0,
    nomeCargo: '',
    cpf: '',
    codigoTipoMov: 0,
    descTipoMov: '',
    horasMensais: 0,
    dataAdmissao: '',
    codigoSetor: 0,
    nomeSetor: '',
    codigoDivisao: 0,
    nomeDivisao: '',
    codigoOrgao: 0,
    nomeOrgao: '',
    codigoCentroCusto: 0,
    nomeCentroCusto: '',
    codigoVinculo: 0,
    nomeVinculo: '',
    padrao: '',
    dataReferencia: '',
    inativo: '',
    pensionista: '',
    tempoServico: '',
    valorRemuneracaoBasica: 0,
    valorVerbasEventuais: 0,
    valorVerbasIndenizatorias: 0,
    valorFerias: 0,
    valorDecimoTerceiro: 0,
    valorDeducoesObrigatorias: 0,
    valorRemuneracao: 0,
    chartPieOrgao: {
        options: {},
        series: []
    },
    chartPieOrgaoValor: {
        options: {},
        series: []
    }   
};

export function servidor(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_SERVIDOR':
            return {
                ...state,
                servidor: action.servidor,
                totalItems: action.totalItems
            };
        case 'FETECHED_ALL_SERVIDOR_CHARTS':
            return {
                ...state,
                charts: action.charts
            };
        case 'SERVIDOR_DETAIL':
            return {
                ...state,
                id: action.id,
                matricula: action.matricula,
                nome: action.nome,
                codigoCargo: action.codigoCargo,
                nomeCargo: action.nomeCargo,
                cpf: action.cpf,
                codigoTipoMov: action.codigoTipoMov,
                descTipoMov: action.descTipoMov,
                horasMensais: action.horasMensais,
                dataAdmissao: action.dataAdmissao,
                codigoSetor: action.codigoSetor,
                nomeSetor: action.nomeSetor,
                codigoDivisao: action.codigoDivisao,
                nomeDivisao: action.nomeDivisao,
                codigoOrgao: action.codigoOrgao,
                nomeOrgao: action.nomeOrgao,
                codigoCentroCusto: action.codigoCentroCusto,
                nomeCentroCusto: action.nomeCentroCusto,
                codigoVinculo: action.codigoVinculo,
                nomeVinculo: action.nomeVinculo,
                padrao: action.padrao,
                dataReferencia: action.dataReferencia,
                inativo: action.inativo,
                pensionista: action.pensionista,
                tempoServico: action.tempoServico,
                valorRemuneracaoBasica: action.valorRemuneracaoBasica,
                valorVerbasEventuais: action.valorVerbasEventuais,
                valorVerbasIndenizatorias: action.valorVerbasIndenizatorias,
                valorFerias: action.valorFerias,
                valorDecimoTerceiro: action.valorDecimoTerceiro,
                valorDeducoesObrigatorias: action.valorDeducoesObrigatorias,
                valorRemuneracao: action.valorRemuneracao
            };
        case 'CHARTPIE_ORGAO_DETAIL':
            return {
                ...state,
                chartPieOrgao: action.chartPieOrgao
            };
        case 'CHARTPIE_ORGAO_VALOR_DETAIL':
            return {
                ...state,
                chartPieOrgaoValor: action.chartPieOrgaoValor
            };
        case "SERVIDOR_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
