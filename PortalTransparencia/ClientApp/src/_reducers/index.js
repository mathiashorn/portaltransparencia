﻿import { combineReducers } from 'redux';

import { convenio } from './convenio.reducer';
import { empenho } from './empenho.reducer';
import { licitacao } from './licitacao.reducer';
import { orgao } from './orgao.reducer';
import { servidor } from './servidor.reducer';

const rootReducer = combineReducers({
    convenio,
    empenho,
    licitacao,
    orgao,
    servidor
});

export default rootReducer;