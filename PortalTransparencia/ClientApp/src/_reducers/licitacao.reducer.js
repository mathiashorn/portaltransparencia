﻿const initialState = {
    anchor: 'left',
    licitacao: [],
    open: false,
    page: 1,
    limit: 5,
    id: 0,
    codigoOrgao: '',
    nrLicitacao: '',
    anoLicitacao: 0,
    cdTipoModalidade: '',
    nrComissao: '',
    anoComissao: '',
    tpComissao: '',
    nrProcesso: '',
    anoProcesso: '',
    tpObjeto: '',
    cdTipoFaseAtual: '',
    tpLicitacao: '',
    tpNivelJulgamento: '',
    dtAutorizacaoAdesao: '',
    tpCaracteristicaObjeto: '',
    tpNatureza: '',
    tpRegimeExecucao: 0,
    blPermiteSubcontratacao: '',
    tpBeneficioMicroEpp: '',
    tpFornecimento: '',
    tpAtuacaoRegistro: 0,
    nrLicitacaoOriginal: '',
    anoLicitacaoOriginal: 0,
    nrAtaRegistroPreco: '',
    dtAtaRegistroPreco: '',
    pcTaxaRisco: 0,
    tpExecucao: 0,
    tpDisputa: 0,
    tpPrequalificacao: 0,
    blInversaoFases: '',
    tpResultadoGlobal: 0,
    cnpjOrgaoGerenciador: '',
    nmOrgaoGerenciador: '',
    dsObjeto: '',
    cdTipoFundamentacao: '',
    nrArtigo: 0,
    dsInciso: '',
    dsLei: '',
    dtInicioInscrCred: 0,
    dtFimInscrCred: 0,
    dtInicioVigenCred: 0,
    dtFimVigenCred: 0,
    vlLicitacao: '',
    blOrcamentoSigiloso: 0,
    blRecebeInscricaoPerVig: 0,
    blPermiteConsorcio: '',
    dtAbertura: '',
    dtHomologacao: '',
    dtAdjudicacao: '',
    blLicitPropriaOrgao: '',
    tpDocumentoFornecedor: 0,
    nrDocumentoFornecedor: '',
    tpDocumentoVencedor: 0,
    nrDocumentoVencedor: '',
    vlHomologado: 0,
    blGeraDespesa: '',
    dsObservacao: '',
    pcTxEstimada: 0,
    pcTxHomologada: 0,
    blCompartilhada: '',
    empenhos: [],
    chartLineAno: {
        options: {},
        series: []
    },
    chartPieModalidade: {
        options: {},
        series: []
    }
};

export function licitacao(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_LICITACAO':
            return {
                ...state,
                licitacao: action.licitacao,
                totalItems: action.totalItems
            };
        case 'LICITACAO_DETAIL':
            return {
                ...state,
                id: action.id,
                codigoOrgao: action.codigoOrgao,
                nrLicitacao: action.nrLicitacao,
                anoLicitacao: action.anoLicitacao,
                cdTipoModalidade: action.cdTipoModalidade,
                nrComissao: action.nrComissao,
                anoComissao: action.anoComissao,
                tpComissao: action.tpComissao,
                nrProcesso: action.nrProcesso,
                anoProcesso: action.anoProcesso,
                tpObjeto: action.tpObjeto,
                cdTipoFaseAtual: action.cdTipoFaseAtual,
                tpLicitacao: action.tpLicitacao,
                tpNivelJulgamento: action.tpNivelJulgamento,
                dtAutorizacaoAdesao: action.dtAutorizacaoAdesao,
                tpCaracteristicaObjeto: action.tpCaracteristicaObjeto,
                tpNatureza: action.tpNatureza,
                tpRegimeExecucao: action.tpRegimeExecucao,
                blPermiteSubcontratacao: action.blPermiteSubcontratacao,
                tpBeneficioMicroEpp: action.tpBeneficioMicroEpp,
                tpFornecimento: action.tpFornecimento,
                tpAtuacaoRegistro: action.tpAtuacaoRegistro,
                nrLicitacaoOriginal: action.nrLicitacaoOriginal,
                anoLicitacaoOriginal: action.anoLicitacaoOriginal,
                nrAtaRegistroPreco: action.nrAtaRegistroPreco,
                dtAtaRegistroPreco: action.dtAtaRegistroPreco,
                pcTaxaRisco: action.pcTaxaRisco,
                tpExecucao: action.tpExecucao,
                tpDisputa: action.tpDisputa,
                tpPrequalificacao: action.tpPrequalificacao,
                blInversaoFases: action.blInversaoFases,
                tpResultadoGlobal: action.tpResultadoGlobal,
                cnpjOrgaoGerenciador: action.cnpjOrgaoGerenciador,
                nmOrgaoGerenciador: action.nmOrgaoGerenciador,
                dsObjeto: action.dsObjeto,
                cdTipoFundamentacao: action.cdTipoFundamentacao,
                nrArtigo: action.nrArtigo,
                dsInciso: action.dsInciso,
                dsLei: action.dsLei,
                dtInicioInscrCred: action.dtInicioInscrCred,
                dtFimInscrCred: action.dtFimInscrCred,
                dtInicioVigenCred: action.dtInicioVigenCred,
                dtFimVigenCred: action.dtFimVigenCred,
                vlLicitacao: action.vlLicitacao,
                blOrcamentoSigiloso: action.blOrcamentoSigiloso,
                blRecebeInscricaoPerVig: action.blRecebeInscricaoPerVig,
                blPermiteConsorcio: action.blPermiteConsorcio,
                dtAbertura: action.dtAbertura,
                dtHomologacao: action.dtHomologacao,
                dtAdjudicacao: action.dtAdjudicacao,
                blLicitPropriaOrgao: action.blLicitPropriaOrgao,
                tpDocumentoFornecedor: action.tpDocumentoFornecedor,
                nrDocumentoFornecedor: action.nrDocumentoFornecedor,
                tpDocumentoVencedor: action.tpDocumentoVencedor,
                nrDocumentoVencedor: action.nrDocumentoVencedor,
                vlHomologado: action.vlHomologado,
                blGeraDespesa: action.blGeraDespesa,
                dsObservacao: action.dsObservacao,
                pcTxEstimada: action.pcTxEstimada,
                pcTxHomologada: action.pcTxHomologada,
                blCompartilhada: action.blCompartilhada,
                empenhos: action.empenhos
            };
        case 'CHARTLINE_ANO_DETAIL':
            return {
                ...state,
                chartLineAno: action.chartLineAno
            };
        case 'CHARTPIE_MODALIDADE_DETAIL':
            return {
                ...state,
                chartPieModalidade: action.chartPieModalidade
            };
        case "LICITACAO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
