﻿const initialState = {
    anchor: 'left',
    empenho: [],
    open: false,
    page: 1,
    limit: 5,
    id: '',
    codigoOrgao: '',
    codigoUnidadeOrcamentaria: '',
    codigoFuncao: '',
    codigoSubfuncao: '',
    codigoPrograma: '',
    codigoProjeto: '',
    codigoRubricaDespesa: '',
    codigoRecursoVinculado: '',
    contrapartidaRecurso: '',
    numeroEmpenho: '',
    dataEmpenho: '',
    valorEmpenho: 0,
    sinalValor: '',
    codigoCredor: '',
    caracteristicaPeculiar: '',
    registroPrecos: '',
    numeroLicitacao: '',
    anoLicitacao: 0,
    historicoEmpenho: '',
    modalidadeLicitacao: '',
    baseLegal: '',
    identificadorDespesaFuncionario: '',
    licitacaoCompartilhada: '',
    cnpjGerenciadorLicitacao: '',
    chartLineAno: {
        options: {},
        series: []
    },
    chartPieOrgao: {
        options: {},
        series: []
    }
};

export function empenho(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_EMPENHO':
            return {
                ...state,
                empenho: action.empenho,
                totalItems: action.totalItems
            };
        case 'EMPENHO_DETAIL':
            return {
                ...state,
                id: action.id,
                codigoOrgao: action.codigoOrgao,
                codigoUnidadeOrcamentaria: action.codigoUnidadeOrcamentaria,
                codigoFuncao: action.codigoFuncao,
                codigoSubfuncao: action.codigoSubfuncao,
                codigoPrograma: action.codigoPrograma,
                codigoProjeto: action.codigoProjeto,
                codigoRubricaDespesa: action.codigoRubricaDespesa,
                codigoRecursoVinculado: action.codigoRecursoVinculado,
                contrapartidaRecurso: action.contrapartidaRecurso,
                numeroEmpenho: action.numeroEmpenho,
                dataEmpenho: action.dataEmpenho,
                valorEmpenho: action.valorEmpenho,
                sinalValor: action.sinalValor,
                codigoCredor: action.codigoCredor,
                caracteristicaPeculiar: action.caracteristicaPeculiar,
                registroPrecos: action.registroPrecos,
                numeroLicitacao: action.numeroLicitacao,
                anoLicitacao: action.anoLicitacao,
                historicoEmpenho: action.historicoEmpenho,
                modalidadeLicitacao: action.modalidadeLicitacao,
                baseLegal: action.baseLegal,
                identificadorDespesaFuncionario: action.identificadorDespesaFuncionario,
                licitacaoCompartilhada: action.licitacaoCompartilhada,
                cnpjGerenciadorLicitacao: action.cnpjGerenciadorLicitacao
            };
        case 'CHARTLINE_ANO_DETAIL':
            return {
                ...state,
                chartLineAno: action.chartLineAno
            };
        case 'CHARTPIE_ORGAO_DETAIL':
            return {
                ...state,
                chartPieOrgao: action.chartPieOrgao
            };
        case "EMPENHO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
