﻿const initialState = {
    anchor: 'left',
    convenio: [],
    open: false,
    page: 1,
    limit: 5,
    convenio1: '',
    administracao: '',
    situacao: '',
    convenente: '',
    concedente: '',
    interveniente: '',
    tipo: '',
    classificacao: '',
    objeto: '',
    dataAssinatura: '',
    dataEncerramento: '',
    dataRescisao: '',
    dataInicio: '',
    dataVencimento: '',
    dataVencimentoOriginal: '',
    recurso: '',
    contrapartida: 0,
    valorRecurso: 0,
    valorContrapartida: 0,
    valorOriginal: 0,
    valorTotal: 0,
    chartLineAno: {
        options: {},
        series: []
    },
    chartPieTipo: {
        options: {},
        series: []
    },
    chartPieClassificacao: {
        options: {},
        series: []
    }
};

export function convenio(state = initialState, action) {
    switch (action.type) {
        case 'FETECHED_ALL_CONVENIO':
            return {
                ...state,
                convenio: action.convenio,
                totalItems: action.totalItems
            };
        case 'CONVENIO_DETAIL':
            return {
                ...state,
                convenio1: action.convenio1,
                administracao: action.administracao,
                situacao: action.situacao,
                convenente: action.convenente,
                concedente: action.concedente,
                interveniente: action.interveniente,
                tipo: action.tipo,
                classificacao: action.classificacao,
                objeto: action.objeto,
                dataAssinatura: action.dataAssinatura,
                dataEncerramento: action.dataEncerramento,
                dataRescisao: action.dataRescisao,
                dataInicio: action.dataInicio,
                dataVencimento: action.dataVencimento,
                dataVencimentoOriginal: action.dataVencimentoOriginal,
                recurso: action.recurso,
                contrapartida: action.contrapartida,
                valorRecurso: action.valorRecurso,
                valorContrapartida: action.valorContrapartida,
                valorOriginal: action.valorOriginal,
                valorTotal: action.valorTotal,
            };
        case 'CHARTLINE_ANO_DETAIL':
            return {
                ...state,
                chartLineAno: action.chartLineAno
            };
        case 'CHARTPIE_TIPO_DETAIL':
            return {
                ...state,
                chartPieTipo: action.chartPieTipo
            };
        case 'CHARTPIE_CLASSIFICACAO_DETAIL':
            return {
                ...state,
                chartPieClassificacao: action.chartPieClassificacao
            };
        case "CONVENIO_UPDATED":
            return state;
        case "HANDLE_ON_CHANGE":
            return {
                ...state,
                [action.props]: action.value
            };
        default:
            return state
    }
}
