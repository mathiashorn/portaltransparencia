﻿import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { empenhoAction } from '../_actions';
import { withRouter } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from '@material-ui/core/FormControl';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const drawerWidth = 0;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
});
class EmpenhoEdit extends Component {

    handleChange = prop => event => {
        const { dispatch } = this.props;
        dispatch(empenhoAction.onChangeProps(prop, event));
    };

    componentDidMount() {
        const { match: { params } } = this.props;
        if (params.id) {            
            const { dispatch } = this.props;
            dispatch(empenhoAction.getEmpenhoById(params.id));
        }
    }

    render() {
        const { classes } = this.props;
        const { match: { params } } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    {/*<Nav />*/}
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        {/*<Grid container spacing={2}>
                            <Grid item xs={3}>
                                <Typography>{'Visualizar Empenho'}</Typography>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">
                            </Grid>
                        </Grid>
                        <br /><br />*/}
                        <Grid container spacing={10}>
                            <Grid item xs={12}>
                                <div>
                                    <Paper className={classes.contentRoot} elevation={1}>
                                        <Breadcrumbs aria-label="breadcrumb">
                                            <Link color="inherit" href="/home" >
                                                Área do Cidadão
                                        </Link>
                                            <Link color="inherit" href="/empenhos/" >
                                                Empenhos
                                        </Link>
                                            <Typography color="textPrimary">Detalhes do Empenho</Typography>
                                        </Breadcrumbs>
                                        <br />
                                        <form className={classes.container}>
                                            <Grid container spacing={1}>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="numeroEmpenho"
                                                            label="Número do Empenho"
                                                            className={classes.textField}
                                                            value={this.props.empenho.numeroEmpenho}
                                                            onChange={this.handleChange('numeroEmpenho')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataEmpenho"
                                                            label="Data do Empenho"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            defaultValue="2017-05-24T10:30"
                                                            value={this.props.empenho.dataEmpenho}
                                                            onChange={this.handleChange('dataEmpenho')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="historicoEmpenho"
                                                            label="Histórico"
                                                            className={classes.textField}
                                                            multiline
                                                            value={this.props.empenho.historicoEmpenho}
                                                            onChange={this.handleChange('historicoEmpenho')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={1}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="sinalValor"
                                                            label="Sinal Valor"
                                                            className={classes.textField}
                                                            value={this.props.empenho.sinalValor}
                                                            onChange={this.handleChange('sinalValor')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorEmpenho"
                                                            label="Valor"
                                                            className={classes.textField}
                                                            value={this.props.empenho.valorEmpenho}
                                                            onChange={this.handleChange('valorEmpenho')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <br />
                                            <Grid container spacing={10}>
                                                <Grid item xs={12} container justify="flex-end">
                                                    <Grid item xs={6} container justify="flex-end">
                                                        <Button variant="contained" color="secondary" className={classes.button} component='a' href="/empenhos">Voltar</Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </form>
                                    </Paper>
                                </div>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
EmpenhoEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return state;
}

const connectedEmpenhoEditPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(EmpenhoEdit)));
export { connectedEmpenhoEditPage as EmpenhoEdit };