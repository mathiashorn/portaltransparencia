﻿import { connect } from 'react-redux';
import { empenhoAction } from '../_actions';
import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from 'react-router-dom';
import Chart from "react-apexcharts";
import Container from '@material-ui/core/Container';

const drawerWidth = 0;
const styles = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    paper: {
        padding: theme.spacing(1),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    titlePaper: {
        padding: theme.spacing(1)
    },
    fixedHeight: {
        height: 240,
    }
});
class EmpenhoChart extends Component {

      componentDidMount() {
        const { dispatch } = this.props;
        dispatch(empenhoAction.getChartLineAno());
        dispatch(empenhoAction.getChartPieOrgao());
    }       
    render() {
        const { classes } = this.props;
        const { match: { params } } = this.props;

        return (
            <div>
                <div>
                    <AppBar />
                    <main className={classes.content}>
                        <div className={classes.appBarSpacer} />
                        <Container maxWidth="lg" className={classes.container}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={12} className={classes.titlePaper}>
                                                <Typography><b>Gráficos dos Empenhos</b></Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography align="left">Número de Empenhos por Ano</Typography>
                                                <Chart
                                                    options={this.props.state.empenho.chartLineAno.options}
                                                    series={this.props.state.empenho.chartLineAno.series}
                                                    type="line"
                                                    height="250"
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography align="left">Valor dos Empenhos por Órgão</Typography>
                                                <Chart
                                                    options={this.props.state.empenho.chartPieOrgao.options}
                                                    series={this.props.state.empenho.chartPieOrgao.series}
                                                    type="donut"
                                                    height="250"
                                                />
                                            </Grid> 
                                        </Grid>
                                    </Paper>
                                </Grid>
                            </Grid>
                        </Container>
                    </main>
                </div>
            </div>
        );
    }
}
EmpenhoChart.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return {
        state
    };
}
const connectedEmpenhoPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(EmpenhoChart)));
export { connectedEmpenhoPage as EmpenhoChart };