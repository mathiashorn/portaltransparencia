﻿import { connect } from 'react-redux';
import { convenioAction } from '../_actions';
import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Icon from '@material-ui/core/Icon';
import Moment from 'react-moment';
import 'moment-timezone';

const drawerWidth = 0;
const styles = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    paper: {
        padding: theme.spacing(3),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    }
});
class Convenio extends Component {

    state = {
        filterModel: {
            page: 0,
            limit: 10,
            convenio1: '',
            dataAssinaturaInicial: '',
            dataAssinaturaFinal: '',
            valorRecursoInicial: 0,
            valorRecursoFinal: 0
        }
    };

    handleChangePageProps(value) {
        var val = value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.page = val;
            return { filterModel };
        });
        this.forceUpdate();
        this.refreshData(this.state.filterModel);
    }
    handleChangeLimitProps(value) {
        var val = value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.limit = val;
            filterModel.page = 0;
            return { filterModel };
        });
        this.forceUpdate();
        this.refreshData(this.state.filterModel);
    }

    componentDidMount() {
        this.refreshData(this.state.filterModel);
    }
    refreshData = (filterModel) => {
        const { dispatch } = this.props;
        dispatch(convenioAction.getConvenioPagination(filterModel));
    }
    handleChangeDataAssinaturaInicial = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.dataAssinaturaInicial = val;
            return { filterModel };
        });
    }
    handleChangeDataAssinaturaFinal = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.dataAssinaturaFinal = val;
            return { filterModel };
        });
    }
    handleChangeConvenio1 = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.convenio1 = val;
            return { filterModel };
        });
    }
    handleChangeConvenente = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.convenente = val;
            return { filterModel };
        });
    }
    handleChangeValorRecursoInicial = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.valorRecursoInicial = val;
            return { filterModel };
        });
    }
    handleChangeValorRecursoFinal = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.valorRecursoFinal = val;
            return { filterModel };
        });
    }
    handleClick = (event, id) => {
        this.state.filterModel.page = 0;
        this.state.filterModel.limit = 10;
        this.refreshData(this.state.filterModel);
    };
    handleExport = (event, id) => {
        const { dispatch } = this.props;
        dispatch(convenioAction.getConvenioCSV(this.state.filterModel));
    };
    render() {

        const { classes } = this.props;
        const { convenio } = this.props.state.convenio;
        const { totalItems } = this.props.state.convenio;
        let { limit, page } = this.state.filterModel;

        const handleChangePage = (event, newPage) => {
            this.handleChangePageProps(newPage);
        };

        const handleChangeRowsPerPage = event => {
            this.handleChangeLimitProps(parseInt(event.target.value, 10));
        };

        return (
            <div>
                <div>
                    <AppBar />
                    <main className={classes.content}>
                        <div className={classes.appBarSpacer} />
                        <Container maxWidth="lg" className={classes.container}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={2}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="dataAssinaturaInicial"
                                                        label="Data Assinatura Inicial"
                                                        className={classes.textField}
                                                        type="date"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeDataAssinaturaInicial('dataAssinaturaInicial')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={2}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="dataAssinaturaFinal"
                                                        label="Data Assinatura Final"
                                                        className={classes.textField}
                                                        type="date"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeDataAssinaturaFinal('dataAssinaturaFinal')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="convenio1"
                                                        label="Número do Convênio"
                                                        className={classes.textField}
                                                        value={this.state.filterModel.convenio1}
                                                        onChange={this.handleChangeConvenio1('convenio1')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="convenente"
                                                        label="Convenente"
                                                        className={classes.textField}
                                                        value={this.state.filterModel.convenente}
                                                        onChange={this.handleChangeConvenente('convenente')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={2}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="valorRecursoInicial"
                                                        label="Valor Recurso Inicial"
                                                        className={classes.textField}
                                                        type="number"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeValorRecursoInicial('valorRecursoInicial')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={2}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="valorRecursoFinal"
                                                        label="Valor Recurso Final"
                                                        className={classes.textField}
                                                        type="number"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeValorRecursoFinal('valorRecursoFinal')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                        </Grid>
                                        <br />
                                        <Grid container spacing={24}>
                                            <Grid item xs={3}>
                                            </Grid>
                                            <Grid item xs={3}>
                                            </Grid>
                                            <Grid item xs={6} container>
                                                <Grid container spacing={24}>
                                                    <Grid item xs={12} container justify="flex-end" >
                                                        <Button variant="contained" color="default" className={classes.button} onClick={(event) => this.handleExport(event)} >Exportar</Button>
                                                        <Button variant="contained" color="primary" className={classes.button} onClick={(event) => this.handleClick(event)} >Pesquisar</Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell>Número do Convênio</TableCell>
                                                    <TableCell>Convenente</TableCell>
                                                    <TableCell>Data da Assinatura</TableCell>
                                                    <TableCell>Ações</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    convenio.map(n => {
                                                        return (
                                                            <TableRow key={n.id}>
                                                                <TableCell component="th" scope="row">
                                                                    {n.convenio1}
                                                                </TableCell>
                                                                <TableCell component="th" scope="row">
                                                                    {n.convenente}
                                                                </TableCell>
                                                                <TableCell>
                                                                    <Moment format="DD/MM/YYYY">
                                                                        {n.dataAssinatura}
                                                                    </Moment>
                                                                </TableCell>
                                                                <TableCell>
                                                                    <IconButton className={classes.button} aria-label="Edit" component='a' href={`/convenio/${n.id}`} target="_blank">
                                                                        <OpenInNewIcon />
                                                                    </IconButton>
                                                                </TableCell>
                                                            </TableRow>
                                                        );
                                                    })
                                                }
                                            </TableBody>
                                            <TableFooter>
                                                <TableRow>
                                                    <TablePagination
                                                        rowsPerPageOptions={[5, 10, 25]}
                                                        colSpan={4}
                                                        count={totalItems}
                                                        rowsPerPage={limit}
                                                        page={page}
                                                        SelectProps={{
                                                            inputProps: { 'aria-label': 'Registros por página' },
                                                            native: true,
                                                        }}
                                                        onChangePage={handleChangePage}
                                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                                        labelRowsPerPage={'Registros por página'}
                                                    />
                                                </TableRow>
                                            </TableFooter>
                                        </Table>
                                    </Paper>
                                </Grid>
                            </Grid>
                        </Container>
                    </main>
                </div>
            </div>
        );
    }
}
Convenio.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return {
        state
    };
}
const connectedConvenioPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Convenio)));
export { connectedConvenioPage as Convenio };