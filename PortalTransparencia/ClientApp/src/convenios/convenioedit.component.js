﻿import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { convenioAction } from '../_actions';
import { withRouter } from 'react-router-dom';
import Select from '@material-ui/core/Select';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from '@material-ui/core/FormControl';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const drawerWidth = 0;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
});
class ConvenioEdit extends Component {

    handleChange = prop => event => {
        const { dispatch } = this.props;
        dispatch(convenioAction.onChangeProps(prop, event));
    };

    componentDidMount() {
        const { match: { params } } = this.props;
        if (params.id) {            
            const { dispatch } = this.props;
            dispatch(convenioAction.getConvenioById(params.id));
        }
    }

    render() {
        const { classes } = this.props;
        const { match: { params } } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    {/*<Nav />*/}
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        {/*<Grid container spacing={2}>
                            <Grid item xs={3}>
                                <Typography>{'Visualizar Convênio'}</Typography>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">
                            </Grid>
                        </Grid>
                        <br /><br />*/}
                        <Grid container spacing={10}>
                            <Grid item xs={12}>
                                <div>
                                    <Paper className={classes.contentRoot} elevation={1}>
                                        <Breadcrumbs aria-label="breadcrumb">
                                            <Link color="inherit" href="/home" >
                                                Área do Cidadão
                                        </Link>
                                            <Link color="inherit" href="/convenios/" >
                                                Convênios
                                        </Link>
                                            <Typography color="textPrimary">Detalhes do Convênio</Typography>
                                        </Breadcrumbs>
                                        <br />
                                        <form className={classes.container}>
                                            <Grid container spacing={1}>
                                            <Grid item xs={12}>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="convenio1"
                                                            label="Número do Convênio"
                                                            className={classes.textField}
                                                            value={this.props.convenio.convenio1}
                                                            onChange={this.handleChange('convenio1')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="convenente"
                                                            label="Convenente"
                                                            className={classes.textField}
                                                            value={this.props.convenio.convenente}
                                                            onChange={this.handleChange('convenente')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="concedente"
                                                            label="Concedente"
                                                            className={classes.textField}
                                                            value={this.props.convenio.concedente}
                                                            onChange={this.handleChange('concedente')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="interveniente"
                                                            label="Interveniente"
                                                            className={classes.textField}
                                                            value={this.props.convenio.interveniente}
                                                            onChange={this.handleChange('interveniente')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="tipo"
                                                            label="Tipo"
                                                            className={classes.textField}
                                                            value={this.props.convenio.tipo}
                                                            onChange={this.handleChange('tipo')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="classificacao"
                                                            label="Classificação"
                                                            className={classes.textField}
                                                            value={this.props.convenio.classificacao}
                                                            onChange={this.handleChange('classificacao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="objeto"
                                                            label="Objeto"
                                                            className={classes.textField}
                                                            multiline
                                                            value={this.props.convenio.objeto}
                                                            onChange={this.handleChange('objeto')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataAssinatura"
                                                            label="Data Assinatura"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            value={this.props.convenio.dataAssinatura}
                                                            onChange={this.handleChange('dataAssinatura')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataEncerramento"
                                                            label="Data Encerramento"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            value={this.props.convenio.dataEncerramento}
                                                            onChange={this.handleChange('dataEncerramento')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataRescisao"
                                                            label="Data Rescisão"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            value={this.props.convenio.dataRescisao}
                                                            onChange={this.handleChange('dataAssinatura')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataInicio"
                                                            label="Data Início"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            value={this.props.convenio.dataInicio}
                                                            onChange={this.handleChange('dataInicio')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataVencimento"
                                                            label="Data Vencimento"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            value={this.props.convenio.dataVencimento}
                                                            onChange={this.handleChange('dataVencimento')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataVencimentoOriginal"
                                                            label="Data Vencimento Original"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            value={this.props.convenio.dataVencimentoOriginal}
                                                            onChange={this.handleChange('dataVencimentoOriginal')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorRecurso"
                                                            label="Valor Recurso"
                                                            className={classes.textField}
                                                            type="number"
                                                            value={this.props.convenio.valorRecurso}
                                                            onChange={this.handleChange('valorRecurso')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorContrapartida"
                                                            label="Valor Contrapartida"
                                                            className={classes.textField}
                                                            value={this.props.convenio.valorContrapartida}
                                                            onChange={this.handleChange('valorContrapartida')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorOriginal"
                                                            label="Valor Original"
                                                            className={classes.textField}
                                                            value={this.props.convenio.valorOriginal}
                                                            onChange={this.handleChange('valorOriginal')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="valorTotal"
                                                            label="Valor Total"
                                                            className={classes.textField}
                                                            value={this.props.convenio.valorTotal}
                                                            onChange={this.handleChange('valorTotal')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <br />
                                            <Grid container spacing={10}>
                                                <Grid item xs={12} container justify="flex-end">
                                                    <Grid item xs={6} container justify="flex-end">
                                                        <Button variant="contained" color="secondary" className={classes.button} component='a' href="/convenios">Voltar</Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </form>
                                    </Paper>
                                </div>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
ConvenioEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return state;
}

const connectedConvenioEditPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(ConvenioEdit)));
export { connectedConvenioEditPage as ConvenioEdit };