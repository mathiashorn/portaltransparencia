import React, { Component } from 'react';
import './App.css';
import { Router, Switch, Route } from 'react-router-dom';
import { Home } from './home/';
import { Convenio } from './convenios/';
import { ConvenioChart } from './convenios/';
import { ConvenioEdit } from './convenios/';
import { Empenho } from './empenhos/';
import { EmpenhoEdit } from './empenhos/';
import { EmpenhoChart } from './empenhos/';
import { Licitacao } from './licitacoes/';
import { LicitacaoChart } from './licitacoes/';
import { LicitacaoEdit } from './licitacoes/';
import { Servidor } from './servidores/';
import { ServidorEdit } from './servidores/';
import { ServidorChart } from './servidores/';
import { Sobre } from './api/';
import { history } from './_helpers';
import { PrivateRoute } from './_components';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Router history={history}>
                    <div>
                        <Switch>
                            <PrivateRoute exact path='/' component={Home} />
                            <PrivateRoute exact path='/home' component={Home} />
                            <PrivateRoute exact path='/convenios' component={Convenio} />
                            <PrivateRoute exact path='/convenios/graficos' component={ConvenioChart} />
                            <PrivateRoute exact path='/convenio' component={ConvenioEdit} />
                            <PrivateRoute exact path='/convenio/:id' component={ConvenioEdit} />
                            <PrivateRoute exact path='/empenhos' component={Empenho} />
                            <PrivateRoute exact path='/empenhos/graficos' component={EmpenhoChart} />
                            <PrivateRoute exact path='/empenho' component={EmpenhoEdit} />
                            <PrivateRoute exact path='/empenho/:id' component={EmpenhoEdit} />
                            <PrivateRoute exact path='/licitacoes' component={Licitacao} />
                            <PrivateRoute exact path='/licitacoes/graficos' component={LicitacaoChart} />
                            <PrivateRoute exact path='/licitacao' component={LicitacaoEdit} />
                            <PrivateRoute exact path='/licitacao/:id' component={LicitacaoEdit} />
                            <PrivateRoute exact path='/servidores' component={Servidor} />
                            <PrivateRoute exact path='/servidores/graficos' component={ServidorChart} />
                            <PrivateRoute exact path='/servidor' component={ServidorEdit} />
                            <PrivateRoute exact path='/servidor/:id' component={ServidorEdit} />
                            <PrivateRoute exact path='/api/docs/sobre' component={Sobre} />
                        </Switch>
                    </div>
                </Router>
            </div>
        );
    }
}
export default App;