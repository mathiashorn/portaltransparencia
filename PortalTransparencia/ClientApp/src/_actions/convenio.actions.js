﻿import { convenioService } from '../_services/';
import { history } from '../_helpers';

export const convenioAction = {
    getConvenio,
    getConvenioPagination,
    getConvenioCSV,
    getConvenioById,
    onChangeProps,
    editConvenioInfo,
    createConvenio,
    deleteConvenioById,
    getChartLineAno,
    getChartPieTipo,
    getChartPieClassificacao
};
function getConvenio() {
    return dispatch => {
        let apiEndpoint = 'convenios';
        convenioService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeConveniosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getConvenioPagination(filterModel) {
    return dispatch => {
        let apiEndpoint = 'convenios' + serialize(filterModel);
        convenioService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeConveniosList(response.data.items, response.data.total));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getConvenioCSV(filterModel) {
    return dispatch => {
        let apiEndpoint = 'convenios/csv' + serialize(filterModel);
        convenioService.get(apiEndpoint)
            .then((response) => {

                var fileDownload = require('js-file-download');
                fileDownload(response.data, 'convenios.csv');

            }).catch((err) => {
                console.log(err);
            })
    };
}
function createConvenio(payload) {
    return dispatch => {
        let apiEndpoint = 'convenios/';
        convenioService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/convenios');
            })
    }
}
function getConvenioById(id) {
    return dispatch => {
        let apiEndpoint = 'convenios/' + id;
        convenioService.get(apiEndpoint)
            .then((response) => {
                dispatch(editConveniosDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editConvenioInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'convenios/' + id;
        convenioService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/convenios');
            })
    }
}
function deleteConvenioById(id) {
    return dispatch => {
        let apiEndpoint = 'convenios/' + id;
        convenioService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteConveniosDetails());
                dispatch(convenioAction.getConvenio());
            })
    };
}
export function changeConveniosList(convenio, totalItems) {
    return {
        type: "FETECHED_ALL_CONVENIO",
        convenio: convenio,
        totalItems: totalItems
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editConveniosDetails(convenio) {
    return {
        type: "CONVENIO_DETAIL",
        id: convenio.id,
        convenio1: convenio.convenio1,
        administracao: convenio.administracao,
        situacao: convenio.situacao,
        convenente: convenio.convenente,
        concedente: convenio.concedente,
        interveniente: convenio.interveniente,
        tipo: convenio.tipo,
        classificacao: convenio.classificacao,
        objeto: convenio.objeto,
        dataAssinatura: convenio.dataAssinatura,
        dataEncerramento: convenio.dataEncerramento,
        dataRescisao: convenio.dataRescisao,
        dataInicio: convenio.dataInicio,
        dataVencimento: convenio.dataVencimento,
        dataVencimentoOriginal: convenio.dataVencimentoOriginal,
        recurso: convenio.recurso,
        contrapartida: convenio.contrapartida,
        valorRecurso: convenio.valorRecurso,
        valorContrapartida: convenio.valorContrapartida,
        valorOriginal: convenio.valorOriginal,
        valorTotal: convenio.valorTotal
    }
}
export function updatedUserInfo() {
    return {
        type: "CONVENIO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "CONVENIO_CREATED_SUCCESSFULLY"
    }
}
export function deleteConveniosDetails() {
    return {
        type: "DELETED_CONVENIO_DETAILS"
    }
}
function serialize(obj) {
    let str = '?' + Object.keys(obj).reduce(function (a, k) {
        a.push(k + '=' + encodeURIComponent(obj[k]));
        return a;
    }, []).join('&');
    return str;
}

// gráficos
function getChartLineAno() {
    return dispatch => {
        let apiEndpoint = 'convenios/charts/ano';
        convenioService.get(apiEndpoint)
            .then((response) => {

                let chartLine = {
                    options: {
                        chart: {
                            id: "basic-bar"
                        },
                        xaxis: {
                            categories: response.data.options
                        }
                    },
                    series: [
                        {
                            name: "Número de Convênios",
                            data: response.data.series
                        }
                    ]
                };

                dispatch(editChartLineAnoDetails(chartLine));

            }).catch((err) => {
                console.log(err);
            })
    };
}
function getChartPieTipo() {
    return dispatch => {
        let apiEndpoint = 'convenios/charts/tipo';
        convenioService.get(apiEndpoint)
            .then((response) => {

                let chartPie = {
                    options: {
                        labels: response.data.options
                    },
                    series: response.data.series
                };

                dispatch(editChartPieTipoDetails(chartPie));

            }).catch((err) => {
                console.log(err);
            })
    };
}
function getChartPieClassificacao() {
    return dispatch => {
        let apiEndpoint = 'convenios/charts/classificacao';
        convenioService.get(apiEndpoint)
            .then((response) => {

                let chartPie = {
                    options: {
                        labels: response.data.options
                    },
                    series: response.data.series
                };

                dispatch(editChartPieClassificacaoDetails(chartPie));

            }).catch((err) => {
                console.log(err);
            })
    };
}

export function editChartLineAnoDetails(chart) {
    return {
        type: "CHARTLINE_ANO_DETAIL",
        chartLineAno: chart
    }
}
export function editChartPieTipoDetails(chart) {
    return {
        type: "CHARTPIE_TIPO_DETAIL",
        chartPieTipo: chart
    }
}
export function editChartPieClassificacaoDetails(chart) {
    return {
        type: "CHARTPIE_CLASSIFICACAO_DETAIL",
        chartPieClassificacao: chart
    }
}