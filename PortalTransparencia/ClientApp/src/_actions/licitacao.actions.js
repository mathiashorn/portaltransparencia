﻿import { licitacaoService } from '../_services/';
import { history } from '../_helpers';

export const licitacaoAction = {
    getLicitacao,
    getLicitacaoPagination,
    getLicitacaoCSV,
    getLicitacaoById,
    onChangeProps,
    editLicitacaoInfo,
    createLicitacao,
    deleteLicitacaoById,
    getChartLineAno,
    getChartPieModalidade
};
function getLicitacao() {
    return dispatch => {
        let apiEndpoint = 'licitacoes';
        licitacaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeLicitacoesList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getLicitacaoPagination(filterModel) {
    return dispatch => {
        let apiEndpoint = 'licitacoes' + serialize(filterModel);
        licitacaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeLicitacoesList(response.data.items, response.data.total));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getLicitacaoCSV(filterModel) {
    return dispatch => {
        let apiEndpoint = 'licitacoes/csv' + serialize(filterModel);
        licitacaoService.get(apiEndpoint)
            .then((response) => {
                
                var fileDownload = require('js-file-download');
                fileDownload(response.data, 'licitacoes.csv');

            }).catch((err) => {
                console.log(err);
            })
    };
}
function createLicitacao(payload) {
    return dispatch => {
        let apiEndpoint = 'licitacoes/';
        licitacaoService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/licitacoes');
            })
    }
}
function getLicitacaoById(id) {
    return dispatch => {
        let apiEndpoint = 'licitacoes/' + id;
        licitacaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(editLicitacoesDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editLicitacaoInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'licitacoes/' + id;
        licitacaoService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/licitacoes');
            })
    }
}
function deleteLicitacaoById(id) {
    return dispatch => {
        let apiEndpoint = 'licitacoes/' + id;
        licitacaoService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteLicitacoesDetails());
                dispatch(licitacaoAction.getLicitacao());
            })
    };
}
export function changeLicitacoesList(licitacao, totalItems) {
    return {
        type: "FETECHED_ALL_LICITACAO",
        licitacao: licitacao,
        totalItems: totalItems
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editLicitacoesDetails(licitacao) {
    return {
        type: "LICITACAO_DETAIL",
        id: licitacao.id,
        codigoOrgao: licitacao.codigoOrgao,
        nrLicitacao: licitacao.nrLicitacao,
        anoLicitacao: licitacao.anoLicitacao,
        cdTipoModalidade: licitacao.cdTipoModalidade,
        nrComissao: licitacao.nrComissao,
        anoComissao: licitacao.anoComissao,
        tpComissao: licitacao.tpComissao,
        nrProcesso: licitacao.nrProcesso,
        anoProcesso: licitacao.anoProcesso,
        tpObjeto: licitacao.tpObjeto,
        cdTipoFaseAtual: licitacao.cdTipoFaseAtual,
        tpLicitacao: licitacao.tpLicitacao,
        tpNivelJulgamento: licitacao.tpNivelJulgamento,
        dtAutorizacaoAdesao: licitacao.dtAutorizacaoAdesao,
        tpCaracteristicaObjeto: licitacao.tpCaracteristicaObjeto,
        tpNatureza: licitacao.tpNatureza,
        tpRegimeExecucao: licitacao.tpRegimeExecucao,
        blPermiteSubcontratacao: licitacao.blPermiteSubcontratacao,
        tpBeneficioMicroEpp: licitacao.tpBeneficioMicroEpp,
        tpFornecimento: licitacao.tpFornecimento,
        tpAtuacaoRegistro: licitacao.tpAtuacaoRegistro,
        nrLicitacaoOriginal: licitacao.nrLicitacaoOriginal,
        anoLicitacaoOriginal: licitacao.anoLicitacaoOriginal,
        nrAtaRegistroPreco: licitacao.nrAtaRegistroPreco,
        dtAtaRegistroPreco: licitacao.dtAtaRegistroPreco,
        pcTaxaRisco: licitacao.pcTaxaRisco,
        tpExecucao: licitacao.tpExecucao,
        tpDisputa: licitacao.tpDisputa,
        tpPrequalificacao: licitacao.tpPrequalificacao,
        blInversaoFases: licitacao.blInversaoFases,
        tpResultadoGlobal: licitacao.tpResultadoGlobal,
        cnpjOrgaoGerenciador: licitacao.cnpjOrgaoGerenciador,
        nmOrgaoGerenciador: licitacao.nmOrgaoGerenciador,
        dsObjeto: licitacao.dsObjeto,
        cdTipoFundamentacao: licitacao.cdTipoFundamentacao,
        nrArtigo: licitacao.nrArtigo,
        dsInciso: licitacao.dsInciso,
        dsLei: licitacao.dsLei,
        dtInicioInscrCred: licitacao.dtInicioInscrCred,
        dtFimInscrCred: licitacao.dtFimInscrCred,
        dtInicioVigenCred: licitacao.dtInicioVigenCred,
        dtFimVigenCred: licitacao.dtFimVigenCred,
        vlLicitacao: licitacao.vlLicitacao,
        blOrcamentoSigiloso: licitacao.blOrcamentoSigiloso,
        blRecebeInscricaoPerVig: licitacao.blRecebeInscricaoPerVig,
        blPermiteConsorcio: licitacao.blPermiteConsorcio,
        dtAbertura: licitacao.dtAbertura,
        dtHomologacao: licitacao.dtHomologacao,
        dtAdjudicacao: licitacao.dtAdjudicacao,
        blLicitPropriaOrgao: licitacao.blLicitPropriaOrgao,
        tpDocumentoFornecedor: licitacao.tpDocumentoFornecedor,
        nrDocumentoFornecedor: licitacao.nrDocumentoFornecedor,
        tpDocumentoVencedor: licitacao.tpDocumentoVencedor,
        nrDocumentoVencedor: licitacao.nrDocumentoVencedor,
        vlHomologado: licitacao.vlHomologado,
        blGeraDespesa: licitacao.blGeraDespesa,
        dsObservacao: licitacao.dsObservacao,
        pcTxEstimada: licitacao.pcTxEstimada,
        pcTxHomologada: licitacao.pcTxHomologada,
        blCompartilhada: licitacao.blCompartilhada,
        empenhos: licitacao.empenhos
    }
}
export function updatedUserInfo() {
    return {
        type: "LICITACAO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "LICITACAO_CREATED_SUCCESSFULLY"
    }
}
export function deleteLicitacoesDetails() {
    return {
        type: "DELETED_LICITACAO_DETAILS"
    }
}
function serialize(obj) {
    let str = '?' + Object.keys(obj).reduce(function (a, k) {
        a.push(k + '=' + encodeURIComponent(obj[k]));
        return a;
    }, []).join('&');
    return str;
}

// gráficos
function getChartLineAno() {
    return dispatch => {
        let apiEndpoint = 'licitacoes/charts/ano';
        licitacaoService.get(apiEndpoint)
            .then((response) => {

                let chartLine = {
                    options: {
                        chart: {
                            id: "basic-bar"
                        },
                        xaxis: {
                            categories: response.data.options
                        }
                    },
                    series: [
                        {
                            name: "Número de Licitações",
                            data: response.data.series
                        }
                    ]
                };

                dispatch(editChartLineAnoDetails(chartLine));

            }).catch((err) => {
                console.log(err);
            })
    };
}
function getChartPieModalidade() {
    return dispatch => {
        let apiEndpoint = 'licitacoes/charts/modalidade';
        licitacaoService.get(apiEndpoint)
            .then((response) => {

                let chartPie = {
                    options: {
                        labels: response.data.options
                    },
                    series: response.data.series
                };

                dispatch(editChartPieModalidadeDetails(chartPie));

            }).catch((err) => {
                console.log(err);
            })
    };
}
export function editChartLineAnoDetails(chart) {
    return {
        type: "CHARTLINE_ANO_DETAIL",
        chartLineAno: chart
    }
}
export function editChartPieModalidadeDetails(chart) {
    return {
        type: "CHARTPIE_MODALIDADE_DETAIL",
        chartPieModalidade: chart
    }
}