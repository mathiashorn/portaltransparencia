﻿import { orgaoService } from '../_services/';
import { history } from '../_helpers';

export const orgaoAction = {
    getOrgao
};
function getOrgao() {
    return dispatch => {
        let apiEndpoint = 'orgaos';
        orgaoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeOrgaosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}   
export function changeOrgaosList(orgao) {
    return {
        type: "FETECHED_ALL_ORGAO",
        orgao: orgao
    }
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}