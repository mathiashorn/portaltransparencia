﻿import { servidorService } from '../_services/';
import { history } from '../_helpers';

export const servidorAction = {
    getServidor,
    getServidorPagination,
    getServidorCSV,
    getServidorCharts,
    getServidorById,
    onChangeProps,
    editServidorInfo,
    createServidor,
    deleteServidorById,
    getChartPieOrgao,
    getChartPieOrgaoValor
};
function getServidor() {
    return dispatch => {
        let apiEndpoint = 'servidores';
        servidorService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeServidorsList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getServidorPagination(filterModel) {
    return dispatch => {
        let apiEndpoint = 'servidores' + serialize(filterModel);
        servidorService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeServidorsList(response.data.items, response.data.total));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getServidorCSV(filterModel) {
    return dispatch => {
        let apiEndpoint = 'servidores/csv' + serialize(filterModel);
        servidorService.get(apiEndpoint)
            .then((response) => {

                var fileDownload = require('js-file-download');
                fileDownload(response.data, 'servidores.csv');

            }).catch((err) => {
                console.log(err);
            })
    };
}
function getServidorCharts() {
    return dispatch => {
        let apiEndpoint = 'servidores/charts';
        servidorService.get(apiEndpoint)
            .then((response) => {
                console.log('response.data', response.data);
                dispatch(changeServidorsCharts(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createServidor(payload) {
    return dispatch => {
        let apiEndpoint = 'servidores/';
        servidorService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/servidores');
            })
    }
}
function getServidorById(id) {
    return dispatch => {
        let apiEndpoint = 'servidores/' + id;
        servidorService.get(apiEndpoint)
            .then((response) => {
                dispatch(editServidorsDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editServidorInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'servidores/' + id;
        servidorService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/servidores');
            })
    }
}
function deleteServidorById(id) {
    return dispatch => {
        let apiEndpoint = 'servidores/' + id;
        servidorService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteServidorsDetails());
                dispatch(servidorAction.getServidor());
            })
    };
}
export function changeServidorsList(servidor, totalItems) {
    return {
        type: "FETECHED_ALL_SERVIDOR",
        servidor: servidor,
        totalItems: totalItems
    }
}
export function changeServidorsCharts(charts) {
    console.log('changeServidorsCharts', charts);
    return {
        type: "FETECHED_ALL_SERVIDOR_CHARTS",
        charts: charts
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editServidorsDetails(servidor) {
    return {
        type: "SERVIDOR_DETAIL",
        id: servidor.id,
        matricula: servidor.matricula,
        nome: servidor.nome,
        codigoCargo: servidor.codigoCargo,
        nomeCargo: servidor.nomeCargo,
        cpf: servidor.cpf,
        codigoTipoMov: servidor.codigoTipoMov,
        descTipoMov: servidor.descTipoMov,
        horasMensais: servidor.horasMensais,
        dataAdmissao: servidor.dataAdmissao,
        codigoSetor: servidor.codigoSetor,
        nomeSetor: servidor.nomeSetor,
        codigoDivisao: servidor.codigoDivisao,
        nomeDivisao: servidor.nomeDivisao,
        codigoOrgao: servidor.codigoOrgao,
        nomeOrgao: servidor.nomeOrgao,
        codigoCentroCusto: servidor.codigoCentroCusto,
        nomeCentroCusto: servidor.nomeCentroCusto,
        codigoVinculo: servidor.codigoVinculo,
        nomeVinculo: servidor.nomeVinculo,
        padrao: servidor.padrao,
        dataReferencia: servidor.dataReferencia,
        inativo: servidor.inativo,
        pensionista: servidor.pensionista,
        tempoServico: servidor.tempoServico,
        valorRemuneracaoBasica: servidor.valorRemuneracaoBasica,
        valorVerbasEventuais: servidor.valorVerbasEventuais,
        valorVerbasIndenizatorias: servidor.valorVerbasIndenizatorias,
        valorFerias: servidor.valorFerias,
        valorDecimoTerceiro: servidor.valorDecimoTerceiro,
        valorDeducoesObrigatorias: servidor.valorDeducoesObrigatorias,
        valorRemuneracao: servidor.valorRemuneracao
    }
}
export function updatedUserInfo() {
    return {
        type: "SERVIDOR_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "SERVIDOR_CREATED_SUCCESSFULLY"
    }
}
export function deleteServidorsDetails() {
    return {
        type: "DELETED_SERVIDOR_DETAILS"
    }
}
function serialize(obj) {
    let str = '?' + Object.keys(obj).reduce(function (a, k) {
        a.push(k + '=' + encodeURIComponent(obj[k]));
        return a;
    }, []).join('&');
    return str;
}

// gráficos
function getChartPieOrgao() {
    return dispatch => {
        let apiEndpoint = 'servidores/charts/orgao';
        servidorService.get(apiEndpoint)
            .then((response) => {

                let chartPie = {
                    options: {
                        labels: response.data.options
                    },
                    series: response.data.series
                };

                dispatch(editChartPieOrgaoDetails(chartPie));

            }).catch((err) => {
                console.log(err);
            })
    };
}
function getChartPieOrgaoValor() {
    return dispatch => {
        let apiEndpoint = 'servidores/charts/orgao/valor';
        servidorService.get(apiEndpoint)
            .then((response) => {

                let chartPie = {
                    options: {
                        labels: response.data.options
                    },
                    series: response.data.series
                };

                dispatch(editChartPieOrgaoValorDetails(chartPie));

            }).catch((err) => {
                console.log(err);
            })
    };
}
export function editChartPieOrgaoDetails(chart) {
    return {
        type: "CHARTPIE_ORGAO_DETAIL",
        chartPieOrgao: chart
    }
}
export function editChartPieOrgaoValorDetails(chart) {
    return {
        type: "CHARTPIE_ORGAO_VALOR_DETAIL",
        chartPieOrgaoValor: chart
    }
}