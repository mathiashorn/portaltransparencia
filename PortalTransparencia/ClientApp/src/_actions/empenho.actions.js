﻿import { empenhoService } from '../_services/';
import { history } from '../_helpers';

export const empenhoAction = {
    getEmpenho,
    getEmpenhoPagination,
    getEmpenhoCSV,
    getEmpenhoCharts,
    getEmpenhoById,
    onChangeProps,
    editEmpenhoInfo,
    createEmpenho,
    deleteEmpenhoById,
    getChartLineAno,
    getChartPieOrgao
};
function getEmpenho() {
    return dispatch => {
        let apiEndpoint = 'empenhos';
        empenhoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeEmpenhosList(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getEmpenhoPagination(filterModel) {
    return dispatch => {
        let apiEndpoint = 'empenhos' + serialize(filterModel);
        empenhoService.get(apiEndpoint)
            .then((response) => {
                dispatch(changeEmpenhosList(response.data.items, response.data.total));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function getEmpenhoCSV(filterModel) {
    return dispatch => {
        let apiEndpoint = 'empenhos/csv' + serialize(filterModel);
        empenhoService.get(apiEndpoint)
            .then((response) => {

                var fileDownload = require('js-file-download');
                fileDownload(response.data, 'empenhos.csv');

            }).catch((err) => {
                console.log(err);
            })
    };
}
function getEmpenhoCharts() {
    return dispatch => {
        let apiEndpoint = 'empenhos/charts';
        empenhoService.get(apiEndpoint)
            .then((response) => {
                console.log('response.data', response.data);
                dispatch(changeEmpenhosCharts(response.data));
            }).catch((err) => {
                console.log(err);
            })
    };
}
function createEmpenho(payload) {
    return dispatch => {
        let apiEndpoint = 'empenhos/';
        empenhoService.post(apiEndpoint, payload)
            .then((response) => {
                dispatch(createUserInfo());
                history.push('/empenhos');
            })
    }
}
function getEmpenhoById(id) {
    return dispatch => {
        let apiEndpoint = 'empenhos/' + id;
        empenhoService.get(apiEndpoint)
            .then((response) => {
                dispatch(editEmpenhosDetails(response.data));
            })
    };
}
function onChangeProps(props, event) {
    return dispatch => {
        dispatch(handleOnChangeProps(props, event.target.value));
    }
}
function editEmpenhoInfo(id, payload) {
    return dispatch => {
        let apiEndpoint = 'empenhos/' + id;
        empenhoService.put(apiEndpoint, payload)
            .then((response) => {
                dispatch(updatedUserInfo());
                history.push('/empenhos');
            })
    }
}
function deleteEmpenhoById(id) {
    return dispatch => {
        let apiEndpoint = 'empenhos/' + id;
        empenhoService.deleteDetail(apiEndpoint)
            .then((response) => {
                dispatch(deleteEmpenhosDetails());
                dispatch(empenhoAction.getEmpenho());
            })
    };
}
export function changeEmpenhosList(empenho, totalItems) {
    return {
        type: "FETECHED_ALL_EMPENHO",
        empenho: empenho,
        totalItems: totalItems
    }
}
export function changeEmpenhosCharts(charts) {
    console.log('changeEmpenhosCharts', charts);
    return {
        type: "FETECHED_ALL_EMPENHO_CHARTS",
        charts: charts
    }
}
export function handleOnChangeProps(props, value) {
    return {
        type: "HANDLE_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editEmpenhosDetails(empenho) {
    return {
        type: "EMPENHO_DETAIL",
        id: empenho.id,
        codigoOrgao: empenho.codigoOrgao,
        codigoUnidadeOrcamentaria: empenho.codigoUnidadeOrcamentaria,
        codigoFuncao: empenho.codigoFuncao,
        codigoSubfuncao: empenho.codigoSubfuncao,
        codigoPrograma: empenho.codigoPrograma,
        codigoProjeto: empenho.codigoProjeto,
        codigoRubricaDespesa: empenho.codigoRubricaDespesa,
        codigoRecursoVinculado: empenho.codigoRecursoVinculado,
        contrapartidaRecurso: empenho.contrapartidaRecurso,
        numeroEmpenho: empenho.numeroEmpenho,
        dataEmpenho: empenho.dataEmpenho,
        valorEmpenho: empenho.valorEmpenho,
        sinalValor: empenho.sinalValor,
        codigoCredor: empenho.codigoCredor,
        caracteristicaPeculiar: empenho.caracteristicaPeculiar,
        registroPrecos: empenho.registroPrecos,
        numeroLicitacao: empenho.numeroLicitacao,
        anoLicitacao: empenho.anoLicitacao,
        historicoEmpenho: empenho.historicoEmpenho,
        modalidadeLicitacao: empenho.modalidadeLicitacao,
        baseLegal: empenho.baseLegal,
        identificadorDespesaFuncionario: empenho.identificadorDespesaFuncionario,
        licitacaoCompartilhada: empenho.licitacaoCompartilhada,
        cnpjGerenciadorLicitacao: empenho.cnpjGerenciadorLicitacao
    }
}
export function updatedUserInfo() {
    return {
        type: "EMPENHO_UPDATED"
    }
}
export function createUserInfo() {
    return {
        type: "EMPENHO_CREATED_SUCCESSFULLY"
    }
}
export function deleteEmpenhosDetails() {
    return {
        type: "DELETED_EMPENHO_DETAILS"
    }
}
function serialize(obj) {
    let str = '?' + Object.keys(obj).reduce(function (a, k) {
        a.push(k + '=' + encodeURIComponent(obj[k]));
        return a;
    }, []).join('&');
    return str;
}

// gráficos
function getChartLineAno() {
    return dispatch => {
        let apiEndpoint = 'empenhos/charts/ano';
        empenhoService.get(apiEndpoint)
            .then((response) => {

                let chartLine = {
                    options: {
                        chart: {
                            id: "basic-bar"
                        },
                        xaxis: {
                            categories: response.data.options
                        }
                    },
                    series: [
                        {
                            name: "Número de Empenhos",
                            data: response.data.series
                        }
                    ]
                };

                dispatch(editChartLineAnoDetails(chartLine));

            }).catch((err) => {
                console.log(err);
            })
    };
}
function getChartPieOrgao() {
    return dispatch => {
        let apiEndpoint = 'empenhos/charts/orgao';
        empenhoService.get(apiEndpoint)
            .then((response) => {

                let chartPie = {
                    options: {
                        labels: response.data.options
                    },
                    series: response.data.series
                };

                dispatch(editChartPieOrgaoDetails(chartPie));

            }).catch((err) => {
                console.log(err);
            })
    };
}
export function editChartLineAnoDetails(chart) {
    return {
        type: "CHARTLINE_ANO_DETAIL",
        chartLineAno: chart
    }
}
export function editChartPieOrgaoDetails(chart) {
    return {
        type: "CHARTPIE_ORGAO_DETAIL",
        chartPieOrgao: chart
    }
}