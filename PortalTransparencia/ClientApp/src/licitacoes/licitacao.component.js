﻿import { connect } from 'react-redux';
import { licitacaoAction } from '../_actions';
import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

const drawerWidth = 0;
const styles = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    paper: {
        padding: theme.spacing(3),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    }
});
class Licitacao extends Component {

    state = {
        filterModel: {
            page: 0,
            limit: 10,
            nrLicitacao: '',
            anoLicitacao: '',
            cdTipoModalidade: '',
            vlLicitacaoInicial: 0,
            vlLicitacaoFinal: 0
        }
    };

    handleChangePageProps(value) {
        var val = value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.page = val;
            return { filterModel };
        });
        this.forceUpdate();
        this.refreshData(this.state.filterModel);
    }

    handleChangeLimitProps(value) {
        var val = value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.limit = val;
            filterModel.page = 0;
            return { filterModel };
        });
        this.forceUpdate();
        this.refreshData(this.state.filterModel);
    }

    componentDidMount() {
        this.refreshData(this.state.filterModel);
    }
    refreshData = (filterModel) => {
        const { dispatch } = this.props;
        dispatch(licitacaoAction.getLicitacaoPagination(filterModel));
    }
    handleChangeNrLicitacao = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.nrLicitacao = val;
            return { filterModel };
        });
    }
    handleChangeAnoLicitacao = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.anoLicitacao = val;
            return { filterModel };
        });
    }
    handleChangeSelectedTipoModalidade(value) {
        var val = value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.cdTipoModalidade = val;
            return { filterModel };
        });
    }
    handleChangeVlLicitacaoInicial = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.vlLicitacaoInicial = val;
            return { filterModel };
        });
    }
    handleChangeVlLicitacaoFinal = prop => event => {
        var val = event.target.value;
        this.setState(prevState => {
            let filterModel = Object.assign({}, prevState.filterModel);
            filterModel.vlLicitacaoFinal = val;
            return { filterModel };
        });
    }
    handleClick = (event, id) => {
        this.state.filterModel.page = 0;
        this.state.filterModel.limit = 10;
        this.refreshData(this.state.filterModel);
    };
    handleExport = (event, id) => {
        const { dispatch } = this.props;
        dispatch(licitacaoAction.getLicitacaoCSV(this.state.filterModel));
    };
    renderOptionsTiposModalidade() {
        var tiposModalidade = [
            { value: 'CPP', name: 'Chamada Pública-PNAE' },
            { value: 'CHP', name: 'Chamamento Público' },
            { value: 'CPC', name: 'Chamamento Público/Credenciamento' },
            { value: 'CNC', name: 'Concorrência' },
            { value: 'CNV', name: 'Convite' },
            { value: 'PRD', name: 'Dispensa' },
            { value: 'ESE', name: 'Licitação lei 13.303/2016 Eletrônico' },
            { value: 'EST', name: 'Licitação lei 13.303/2016 Presencial' },
            { value: 'MAI', name: 'Manifestação de Interesse' },
            { value: 'PRE', name: 'Pregão Eletrônico' },
            { value: 'PRP', name: 'Pregão Presencial' },
            { value: 'PRI', name: 'Processo de Inexigibilidade' },
            { value: 'RDE', name: 'RDC Eletrônico' },
            { value: 'RDC', name: 'RDC Presencial' },
            { value: 'RPO', name: 'Registro de Preços de Outro Órgão' },
            { value: 'RIN', name: 'Regras Internacionais' }
        ];
        return tiposModalidade.map((p, i) => {
            return (
                <MenuItem
                    label="Selecione ..."
                    value={p.value}
                    key={i} name={p.name}>{p.name}
                </MenuItem>
            );
        });
    };
    render() {

        const { classes } = this.props;
        const { licitacao } = this.props.state.licitacao;
        const { totalItems } = this.props.state.licitacao;
        let { limit, page } = this.state.filterModel;

        const handleChangePage = (event, newPage) => {
            this.handleChangePageProps(newPage);
        };

        const handleChangeRowsPerPage = event => {
            this.handleChangeLimitProps(parseInt(event.target.value, 10));
        };

        return (
            <div>
                <div>
                    <AppBar />
                    <main className={classes.content}>
                        <div className={classes.appBarSpacer} />
                        <Container maxWidth="lg" className={classes.container}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={2}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="anoLicitacao"
                                                        label="Ano da Licitação"
                                                        className={classes.textField}
                                                        value={this.state.filterModel.anoLicitacao}
                                                        onChange={this.handleChangeAnoLicitacao('anoLicitacao')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={4}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="nrLicitacao"
                                                        label="Número da Licitação"
                                                        className={classes.textField}
                                                        value={this.state.filterModel.nrLicitacao}
                                                        onChange={this.handleChangeNrLicitacao('nrLicitacao')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="vlLicitacaoInicial"
                                                        label="Valor Licitação Inicial"
                                                        className={classes.textField}
                                                        type="number"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeVlLicitacaoInicial('vlLicitacaoInicial')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <FormControl fullWidth>
                                                    <TextField
                                                        id="vlLicitacaoFinal"
                                                        label="Valor Licitação Final"
                                                        className={classes.textField}
                                                        type="number"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        onChange={this.handleChangeVlLicitacaoFinal('vlLicitacaoFinal')}
                                                        margin="normal"
                                                    />
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="tipo">Modalidade</InputLabel>
                                                    <Select
                                                        name="tipo"
                                                        value={this.state.filterModel.cdTipoModalidade}
                                                        onChange={event => this.handleChangeSelectedTipoModalidade(event.target.value)}
                                                        input={<Input id="cdTipoModalidade" />}
                                                    >
                                                        {this.renderOptionsTiposModalidade()}
                                                    </Select>
                                                    {/*<FormHelperText>Label + placeholder</FormHelperText>*/}
                                                </FormControl>
                                            </Grid>                                            
                                        </Grid>
                                        <br />
                                        <Grid container spacing={24}>
                                            <Grid item xs={3}>
                                            </Grid>
                                            <Grid item xs={6}>
                                            </Grid>
                                            <Grid item xs={3} container justify="center">
                                                <Grid container spacing={24}>
                                                    <Grid item xs={12} container justify="flex-end">
                                                        <Button variant="contained" color="default" className={classes.button} onClick={(event) => this.handleExport(event)} >Exportar</Button>
                                                        <Button variant="contained" color="primary" className={classes.button} onClick={(event) => this.handleClick(event)}>Pesquisar</Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>
                                        <Table className={classes.table}>
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell>Número da Licitação</TableCell>
                                                    <TableCell>Ano da Licitação</TableCell>
                                                    <TableCell>Modalidade</TableCell>
                                                    <TableCell>Ações</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    licitacao.map(n => {

                                                        var modalidade = '';
                                                        if (n.cdTipoModalidade == 'CHP') {
                                                            modalidade = 'Chamamento Público';
                                                        } else if (n.cdTipoModalidade == 'CNC') {
                                                            modalidade = 'Concorrência';
                                                        } else if (n.cdTipoModalidade == 'CNV') {
                                                            modalidade = 'Convite';
                                                        } else if (n.cdTipoModalidade == 'CPC') {
                                                            modalidade = 'Chamamento Público/Credenciamento';
                                                        } else if (n.cdTipoModalidade == 'CPP') {
                                                            modalidade = 'Chamada Pública-PNAE';
                                                        } else if (n.cdTipoModalidade == 'ESE') {
                                                            modalidade = 'Licitação lei 13.303/2016 Eletrônico';
                                                        } else if (n.cdTipoModalidade == 'EST') {
                                                            modalidade = 'Licitação lei 13.303/2016 Presencial';
                                                        } else if (n.cdTipoModalidade == 'MAI') {
                                                            modalidade = 'Manifestação de Interesse';
                                                        } else if (n.cdTipoModalidade == 'PRD') {
                                                            modalidade = 'Dispensa';
                                                        } else if (n.cdTipoModalidade == 'PRE') {
                                                            modalidade = 'Pregão Eletrônico';
                                                        } else if (n.cdTipoModalidade == 'PRI') {
                                                            modalidade = 'Processo de Inexigibilidade';
                                                        } else if (n.cdTipoModalidade == 'PRP') {
                                                            modalidade = 'Pregão Presencial';
                                                        } else if (n.cdTipoModalidade == 'RDE') {
                                                            modalidade = 'RDC Eletrônico';
                                                        } else if (n.cdTipoModalidade == 'RDC') {
                                                            modalidade = 'RDC Presencial';
                                                        } else if (n.cdTipoModalidade == 'RIN') {
                                                            modalidade = 'Regras Internacionais';
                                                        } else if (n.cdTipoModalidade == 'RPO') {
                                                            modalidade = 'Registro de Preços de Outro Órgão';
                                                        } else if (n.cdTipoModalidade == 'TMP') {
                                                            modalidade = 'Tomada de Preços'; 
                                                        } else {
                                                            modalidade = n.cdTipoModalidade;
                                                        }

                                                        return (
                                                            <TableRow key={n.id}>
                                                                <TableCell component="th" scope="row">
                                                                    {n.nrLicitacao}
                                                                </TableCell>
                                                                <TableCell>{n.anoLicitacao}</TableCell>
                                                                <TableCell>{modalidade}</TableCell>
                                                                <TableCell>
                                                                    <IconButton className={classes.button} aria-label="Edit" component='a' href={`/licitacao/${n.id}`} target="_blank">
                                                                        <OpenInNewIcon />
                                                                    </IconButton>
                                                                </TableCell>
                                                            </TableRow>
                                                        );
                                                    })
                                                }
                                            </TableBody>
                                            <TableFooter>
                                                <TableRow>
                                                    <TablePagination
                                                        rowsPerPageOptions={[5, 10, 25]}
                                                        colSpan={4}
                                                        count={totalItems}
                                                        rowsPerPage={limit}
                                                        page={page}
                                                        SelectProps={{
                                                            inputProps: { 'aria-label': 'Registros por página' },
                                                            native: true,
                                                        }}
                                                        onChangePage={handleChangePage}
                                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                                        labelRowsPerPage={'Registros por página'}
                                                    />
                                                </TableRow>
                                            </TableFooter>
                                        </Table>
                                    </Paper>
                                </Grid>
                            </Grid>
                        </Container>
                    </main>
                </div>
            </div>
        );
    }
}
Licitacao.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return {
        state
    };
}
const connectedLicitacaoPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Licitacao)));
export { connectedLicitacaoPage as Licitacao };