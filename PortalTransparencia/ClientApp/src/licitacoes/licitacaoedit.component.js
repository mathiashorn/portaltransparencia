﻿import React, { Component } from 'react';
import AppBar from '../_components/appbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { licitacaoAction } from '../_actions';
import { withRouter } from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const drawerWidth = 0;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
});
class LicitacaoEdit extends Component {

    handleChange = prop => event => {
        const { dispatch } = this.props;
        dispatch(licitacaoAction.onChangeProps(prop, event));
    };

    componentDidMount() {
        const { match: { params } } = this.props;
        if (params.id) {
            const { dispatch } = this.props;
            dispatch(licitacaoAction.getLicitacaoById(params.id));
        }
    }

    render() {
        const { classes } = this.props;
        const { match: { params } } = this.props;

        var modalidade = '';
        if (this.props.licitacao.cdTipoModalidade == 'CHP') {
            modalidade = 'Chamamento Público';
        } else if (this.props.licitacao.cdTipoModalidade == 'CNC') {
            modalidade = 'Concorrência';
        } else if (this.props.licitacao.cdTipoModalidade == 'CNV') {
            modalidade = 'Convite';
        } else if (this.props.licitacao.cdTipoModalidade == 'CPC') {
            modalidade = 'Chamamento Público/Credenciamento';
        } else if (this.props.licitacao.cdTipoModalidade == 'CPP') {
            modalidade = 'Chamada Pública-PNAE';
        } else if (this.props.licitacao.cdTipoModalidade == 'ESE') {
            modalidade = 'Licitação lei 13.303/2016 Eletrônico';
        } else if (this.props.licitacao.cdTipoModalidade == 'EST') {
            modalidade = 'Licitação lei 13.303/2016 Presencial';
        } else if (this.props.licitacao.cdTipoModalidade == 'MAI') {
            modalidade = 'Manifestação de Interesse';
        } else if (this.props.licitacao.cdTipoModalidade == 'PRD') {
            modalidade = 'Dispensa';
        } else if (this.props.licitacao.cdTipoModalidade == 'PRE') {
            modalidade = 'Pregão Eletrônico';
        } else if (this.props.licitacao.cdTipoModalidade == 'PRI') {
            modalidade = 'Processo de Inexigibilidade';
        } else if (this.props.licitacao.cdTipoModalidade == 'PRP') {
            modalidade = 'Pregão Presencial';
        } else if (this.props.licitacao.cdTipoModalidade == 'RDE') {
            modalidade = 'RDC Eletrônico';
        } else if (this.props.licitacao.cdTipoModalidade == 'RDC') {
            modalidade = 'RDC Presencial';
        } else if (this.props.licitacao.cdTipoModalidade == 'RIN') {
            modalidade = 'Regras Internacionais';
        } else if (this.props.licitacao.cdTipoModalidade == 'RPO') {
            modalidade = 'Registro de Preços de Outro Órgão';
        } else if (this.props.licitacao.cdTipoModalidade == 'TMP') {
            modalidade = 'Tomada de Preços';
        } else {
            modalidade = this.props.licitacao.cdTipoModalidade;
        }

        return (           

            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    {/*<Nav />*/}
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        {/*<Grid container spacing={2}>
                            <Grid item xs={3}>
                                <Typography>{'Visualizar Licitação'}</Typography>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3}>
                                <Typography>{'Registros relacionados'}</Typography>
                            </Grid>
                        </Grid>
                        <br /><br />*/}
                        <Grid container>
                            <Grid container item xs={9}>
                                    <Paper className={classes.contentRoot} elevation={1}>
                                        <Breadcrumbs aria-label="breadcrumb">
                                            <Link color="inherit" href="/home" >
                                                Área do Cidadão
                                        </Link>
                                            <Link color="inherit" href="/licitacoes/" >
                                                Licitações
                                        </Link>
                                            <Typography color="textPrimary">Detalhes da Licitação</Typography>
                                        </Breadcrumbs>
                                        <br />
                                        <form className={classes.container}>
                                                <Grid container spacing={1}>
                                                    <Grid item xs={4}>
                                                        <FormControl fullWidth>
                                                            <TextField
                                                                id="nrLicitacao"
                                                                label="Número da Licitação"
                                                                className={classes.textField}
                                                                value={this.props.licitacao.nrLicitacao}
                                                                onChange={this.handleChange('nrLicitacao')}
                                                                margin="normal"
                                                            />
                                                        </FormControl>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="anoLicitacao"
                                                            label="Ano"
                                                            className={classes.textField}
                                                            value={this.props.licitacao.anoLicitacao}
                                                            onChange={this.handleChange('anoLicitacao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dataLicitacao"
                                                            label="Data da Licitação"
                                                            className={classes.textField}
                                                            type="datetime-local"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                            defaultValue="2017-05-24T10:30"
                                                            value={this.props.licitacao.dataLicitacao}
                                                            onChange={this.handleChange('dataLicitacao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="cdTipoModalidade"
                                                            label="Modalidade"
                                                            className={classes.textField}
                                                            value={modalidade}
                                                            onChange={this.handleChange('cdTipoModalidade')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="dsObjeto"
                                                            label="Descrição do Objeto"
                                                            className={classes.textField}
                                                            multiline
                                                            value={this.props.licitacao.dsObjeto}
                                                            onChange={this.handleChange('dsObjeto')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <FormControl fullWidth>
                                                        <TextField
                                                            id="vlLicitacao"
                                                            label="Valor"
                                                            className={classes.textField}
                                                            value={this.props.licitacao.vlLicitacao}
                                                            onChange={this.handleChange('vlLicitacao')}
                                                            margin="normal"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <br />
                                            <Grid container spacing={10}>
                                                <Grid item xs={12} container justify="flex-end">
                                                    <Grid item xs={6} container justify="flex-end">
                                                        <Button variant="contained" color="secondary" className={classes.button} component='a' href="/licitacoes">Voltar</Button>
                                                    </Grid>
                                                </Grid>
                                            </Grid>                                            
                                        </form>
                                    </Paper>
                            </Grid>
                            <Grid container item xs={3} >
                                <Paper className={classes.contentRoot} elevation={1}>
                                    <Breadcrumbs aria-label="breadcrumb">
                                        <Typography color="textPrimary">Registros relacionados</Typography>
                                    </Breadcrumbs>
                                    <Table className={classes.table}>
                                        <TableBody>
                                            {
                                                this.props.licitacao.empenhos.map(n => {
                                                    return (
                                                        <TableRow key={n.id}>
                                                            <TableCell component="th" scope="row">
                                                                <Link href={`/empenho/${n.id}`} className={classes.link} target="blank">
                                                                    Empenho {n.numeroEmpenho}
                                                                </Link>
                                                                    
                                                            </TableCell>
                                                        </TableRow>
                                                    );
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </Paper>
                            </Grid>
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}
LicitacaoEdit.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => {
    return state;
}

const connectedLicitacaoEditPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(LicitacaoEdit)));
export { connectedLicitacaoEditPage as LicitacaoEdit };