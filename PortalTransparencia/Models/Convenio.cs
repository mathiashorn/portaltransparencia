﻿using System;
using System.Collections.Generic;

namespace PortalTransparencia.Models
{
    public partial class Convenio
    {
        public int Id { get; set; }
        public string Convenio1 { get; set; }
        public string Administracao { get; set; }
        public char? Situacao { get; set; }
        public string Convenente { get; set; }
        public string Concedente { get; set; }
        public string Interveniente { get; set; }
        public string Tipo { get; set; }
        public string Classificacao { get; set; }
        public string Objeto { get; set; }
        public DateTime? DataAssinatura { get; set; }
        public DateTime? DataEncerramento { get; set; }
        public DateTime? DataRescisao { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataVencimento { get; set; }
        public DateTime? DataVencimentoOriginal { get; set; }
        public string Recurso { get; set; }
        public string Contrapartida { get; set; }
        public decimal? ValorRecurso { get; set; }
        public decimal? ValorContrapartida { get; set; }
        public decimal? ValorOriginal { get; set; }
        public decimal? ValorTotal { get; set; }
    }
}
