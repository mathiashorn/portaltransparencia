﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PortalTransparencia.Models
{
    public partial class PortalTransparenciaContext : DbContext
    {
        public PortalTransparenciaContext()
        {
        }

        public PortalTransparenciaContext(DbContextOptions<PortalTransparenciaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Convenio> Convenio { get; set; }
        public virtual DbSet<Empenho> Empenho { get; set; }
        public virtual DbSet<Licitacao> Licitacao { get; set; }
        public virtual DbSet<Orgao> Orgao { get; set; }
        public virtual DbSet<Servidor> Servidor { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Database=PortalTransparencia;Username=postgres;Password=postgres");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Convenio>(entity =>
            {
                entity.ToTable("convenio");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('convenio_seq'::regclass)");

                entity.Property(e => e.Administracao)
                    .HasColumnName("administracao")
                    .HasMaxLength(50);

                entity.Property(e => e.Classificacao)
                    .HasColumnName("classificacao")
                    .HasMaxLength(50);

                entity.Property(e => e.Concedente)
                    .HasColumnName("concedente")
                    .HasMaxLength(100);

                entity.Property(e => e.Contrapartida)
                    .HasColumnName("contrapartida")
                    .HasMaxLength(50);

                entity.Property(e => e.Convenente)
                    .HasColumnName("convenente")
                    .HasMaxLength(100);

                entity.Property(e => e.Convenio1)
                    .HasColumnName("convenio")
                    .HasMaxLength(11);

                entity.Property(e => e.DataAssinatura)
                    .HasColumnName("data_assinatura")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DataEncerramento)
                    .HasColumnName("data_encerramento")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DataInicio)
                    .HasColumnName("data_inicio")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DataRescisao)
                    .HasColumnName("data_rescisao")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DataVencimento)
                    .HasColumnName("data_vencimento")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DataVencimentoOriginal)
                    .HasColumnName("data_vencimento_original")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.Interveniente)
                    .HasColumnName("interveniente")
                    .HasMaxLength(100);

                entity.Property(e => e.Objeto).HasColumnName("objeto");

                entity.Property(e => e.Recurso)
                    .HasColumnName("recurso")
                    .HasMaxLength(100);

                entity.Property(e => e.Situacao).HasColumnName("situacao");

                entity.Property(e => e.Tipo)
                    .HasColumnName("tipo")
                    .HasMaxLength(50);

                entity.Property(e => e.ValorContrapartida)
                    .HasColumnName("valor_contrapartida")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorOriginal)
                    .HasColumnName("valor_original")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorRecurso)
                    .HasColumnName("valor_recurso")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorTotal)
                    .HasColumnName("valor_total")
                    .HasColumnType("numeric(12,2)");
            });

            modelBuilder.Entity<Empenho>(entity =>
            {
                entity.ToTable("empenho");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('empenho_seq'::regclass)");

                entity.Property(e => e.AnoLicitacao).HasColumnName("ano_licitacao");

                entity.Property(e => e.BaseLegal)
                    .HasColumnName("base_legal")
                    .HasColumnType("character(2)");

                entity.Property(e => e.CaracteristicaPeculiar)
                    .HasColumnName("caracteristica_peculiar")
                    .HasColumnType("character(3)");

                entity.Property(e => e.CnpjGerenciadorLicitacao)
                    .HasColumnName("cnpj_gerenciador_licitacao")
                    .HasMaxLength(14);

                entity.Property(e => e.CodigoCredor)
                    .HasColumnName("codigo_credor")
                    .HasMaxLength(10);

                entity.Property(e => e.CodigoFuncao)
                    .HasColumnName("codigo_funcao")
                    .HasColumnType("character(2)");

                entity.Property(e => e.CodigoPrograma)
                    .HasColumnName("codigo_programa")
                    .HasColumnType("character(4)");

                entity.Property(e => e.CodigoProjeto)
                    .HasColumnName("codigo_projeto")
                    .HasColumnType("character(5)");

                entity.Property(e => e.CodigoRecursoVinculado)
                    .HasColumnName("codigo_recurso_vinculado")
                    .HasColumnType("character(4)");

                entity.Property(e => e.CodigoRubricaDespesa)
                    .HasColumnName("codigo_rubrica_despesa")
                    .HasMaxLength(15);

                entity.Property(e => e.CodigoSubfuncao)
                    .HasColumnName("codigo_subfuncao")
                    .HasColumnType("character(3)");

                entity.Property(e => e.CodigoUnidadeOrcamentaria)
                    .HasColumnName("codigo_unidade_orcamentaria")
                    .HasColumnType("character(2)");

                entity.Property(e => e.ContrapartidaRecurso)
                    .HasColumnName("contrapartida_recurso")
                    .HasColumnType("character(4)");

                entity.Property(e => e.DataEmpenho)
                    .HasColumnName("data_empenho")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.HistoricoEmpenho).HasColumnName("historico_empenho");

                entity.Property(e => e.IdentificadorDespesaFuncionario).HasColumnName("identificador_despesa_funcionario");

                entity.Property(e => e.LicitacaoCompartilhada).HasColumnName("licitacao_compartilhada");

                entity.Property(e => e.ModalidadeLicitacao)
                    .HasColumnName("modalidade_licitacao")
                    .HasColumnType("character(3)");

                entity.Property(e => e.NumeroEmpenho)
                    .HasColumnName("numero_empenho")
                    .HasMaxLength(13);

                entity.Property(e => e.NumeroLicitacao)
                    .HasColumnName("numero_licitacao")
                    .HasMaxLength(20);

                entity.Property(e => e.OrgaoId).HasColumnName("orgao_id");

                entity.Property(e => e.RegistroPrecos).HasColumnName("registro_precos");

                entity.Property(e => e.SinalValor).HasColumnName("sinal_valor");

                entity.Property(e => e.ValorEmpenho)
                    .HasColumnName("valor_empenho")
                    .HasColumnType("numeric(12,2)");

                entity.HasOne(d => d.Orgao)
                    .WithMany(p => p.Empenho)
                    .HasForeignKey(d => d.OrgaoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_empenho_orgao");
            });

            modelBuilder.Entity<Licitacao>(entity =>
            {
                entity.ToTable("licitacao");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('licitacao_seq'::regclass)");

                entity.Property(e => e.AnoComissao).HasColumnName("ano_comissao");

                entity.Property(e => e.AnoLicitacao).HasColumnName("ano_licitacao");

                entity.Property(e => e.AnoLicitacaoOriginal).HasColumnName("ano_licitacao_original");

                entity.Property(e => e.AnoProcesso).HasColumnName("ano_processo");

                entity.Property(e => e.BlCompartilhada).HasColumnName("bl_compartilhada");

                entity.Property(e => e.BlGeraDespesa).HasColumnName("bl_gera_despesa");

                entity.Property(e => e.BlInversaoFases).HasColumnName("bl_inversao_fases");

                entity.Property(e => e.BlLicitPropriaOrgao).HasColumnName("bl_licit_propria_orgao");

                entity.Property(e => e.BlOrcamentoSigiloso).HasColumnName("bl_orcamento_sigiloso");

                entity.Property(e => e.BlPermiteConsorcio).HasColumnName("bl_permite_consorcio");

                entity.Property(e => e.BlPermiteSubcontratacao).HasColumnName("bl_permite_subcontratacao");

                entity.Property(e => e.BlRecebeInscricaoPerVig).HasColumnName("bl_recebe_inscricao_per_vig");

                entity.Property(e => e.CdTipoFaseAtual)
                    .HasColumnName("cd_tipo_fase_atual")
                    .HasColumnType("character(3)");

                entity.Property(e => e.CdTipoFundamentacao)
                    .HasColumnName("cd_tipo_fundamentacao")
                    .HasMaxLength(8);

                entity.Property(e => e.CdTipoModalidade)
                    .HasColumnName("cd_tipo_modalidade")
                    .HasColumnType("character(3)");

                entity.Property(e => e.CnpjOrgaoGerenciador)
                    .HasColumnName("cnpj_orgao_gerenciador")
                    .HasMaxLength(14);

                entity.Property(e => e.CodigoOrgao)
                    .HasColumnName("codigo_orgao")
                    .HasColumnType("character(5)");

                entity.Property(e => e.DsInciso)
                    .HasColumnName("ds_inciso")
                    .HasMaxLength(10);

                entity.Property(e => e.DsLei)
                    .HasColumnName("ds_lei")
                    .HasMaxLength(10);

                entity.Property(e => e.DsObjeto).HasColumnName("ds_objeto");

                entity.Property(e => e.DsObservacao).HasColumnName("ds_observacao");

                entity.Property(e => e.DtAbertura)
                    .HasColumnName("dt_abertura")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtAdjudicacao)
                    .HasColumnName("dt_adjudicacao")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtAtaRegistroPreco)
                    .HasColumnName("dt_ata_registro_preco")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtAutorizacaoAdesao)
                    .HasColumnName("dt_autorizacao_adesao")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtFimInscrCred)
                    .HasColumnName("dt_fim_inscr_cred")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtFimVigenCred)
                    .HasColumnName("dt_fim_vigen_cred")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtHomologacao)
                    .HasColumnName("dt_homologacao")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtInicioInscrCred)
                    .HasColumnName("dt_inicio_inscr_cred")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DtInicioVigenCred)
                    .HasColumnName("dt_inicio_vigen_cred")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.NmOrgaoGerenciador)
                    .HasColumnName("nm_orgao_gerenciador")
                    .HasMaxLength(60);

                entity.Property(e => e.NrArtigo).HasColumnName("nr_artigo");

                entity.Property(e => e.NrAtaRegistroPreco)
                    .HasColumnName("nr_ata_registro_preco")
                    .HasMaxLength(20);

                entity.Property(e => e.NrComissao).HasColumnName("nr_comissao");

                entity.Property(e => e.NrDocumentoFornecedor)
                    .HasColumnName("nr_documento_fornecedor")
                    .HasMaxLength(14);

                entity.Property(e => e.NrDocumentoVencedor)
                    .HasColumnName("nr_documento_vencedor")
                    .HasMaxLength(14);

                entity.Property(e => e.NrLicitacao)
                    .HasColumnName("nr_licitacao")
                    .HasMaxLength(20);

                entity.Property(e => e.NrLicitacaoOriginal)
                    .HasColumnName("nr_licitacao_original")
                    .HasMaxLength(20);

                entity.Property(e => e.NrProcesso)
                    .HasColumnName("nr_processo")
                    .HasMaxLength(20);

                entity.Property(e => e.PcTaxaRisco)
                    .HasColumnName("pc_taxa_risco")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.PcTxEstimada)
                    .HasColumnName("pc_tx_estimada")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.PcTxHomologada)
                    .HasColumnName("pc_tx_homologada")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.TpAtuacaoRegistro).HasColumnName("tp_atuacao_registro");

                entity.Property(e => e.TpBeneficioMicroEpp).HasColumnName("tp_beneficio_micro_epp");

                entity.Property(e => e.TpCaracteristicaObjeto)
                    .HasColumnName("tp_caracteristica_objeto")
                    .HasColumnType("character(2)");

                entity.Property(e => e.TpComissao).HasColumnName("tp_comissao");

                entity.Property(e => e.TpDisputa).HasColumnName("tp_disputa");

                entity.Property(e => e.TpDocumentoFornecedor).HasColumnName("tp_documento_fornecedor");

                entity.Property(e => e.TpDocumentoVencedor).HasColumnName("tp_documento_vencedor");

                entity.Property(e => e.TpExecucao).HasColumnName("tp_execucao");

                entity.Property(e => e.TpFornecimento).HasColumnName("tp_fornecimento");

                entity.Property(e => e.TpLicitacao)
                    .HasColumnName("tp_licitacao")
                    .HasColumnType("character(3)");

                entity.Property(e => e.TpNatureza).HasColumnName("tp_natureza");

                entity.Property(e => e.TpNivelJulgamento).HasColumnName("tp_nivel_julgamento");

                entity.Property(e => e.TpObjeto)
                    .HasColumnName("tp_objeto")
                    .HasColumnType("character(3)");

                entity.Property(e => e.TpPrequalificacao).HasColumnName("tp_prequalificacao");

                entity.Property(e => e.TpRegimeExecucao).HasColumnName("tp_regime_execucao");

                entity.Property(e => e.TpResultadoGlobal).HasColumnName("tp_resultado_global");

                entity.Property(e => e.VlHomologado)
                    .HasColumnName("vl_homologado")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.VlLicitacao)
                    .HasColumnName("vl_licitacao")
                    .HasColumnType("numeric(12,2)");
            });

            modelBuilder.Entity<Orgao>(entity =>
            {
                entity.ToTable("orgao");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(80);
            });

            modelBuilder.Entity<Servidor>(entity =>
            {
                entity.ToTable("servidor");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('servidor_seq'::regclass)");

                entity.Property(e => e.CodigoCargo).HasColumnName("codigo_cargo");

                entity.Property(e => e.CodigoCentroCusto).HasColumnName("codigo_centro_custo");

                entity.Property(e => e.CodigoDivisao).HasColumnName("codigo_divisao");

                entity.Property(e => e.CodigoOrgao).HasColumnName("codigo_orgao");

                entity.Property(e => e.CodigoSetor).HasColumnName("codigo_setor");

                entity.Property(e => e.CodigoTipoMov).HasColumnName("codigo_tipo_mov");

                entity.Property(e => e.CodigoVinculo).HasColumnName("codigo_vinculo");

                entity.Property(e => e.Cpf)
                    .HasColumnName("cpf")
                    .HasMaxLength(14);

                entity.Property(e => e.DataAdmissao)
                    .HasColumnName("data_admissao")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DataReferencia)
                    .HasColumnName("data_referencia")
                    .HasColumnType("timestamp(0) without time zone");

                entity.Property(e => e.DescTipoMov)
                    .HasColumnName("desc_tipo_mov")
                    .HasMaxLength(50);

                entity.Property(e => e.HorasMensais).HasColumnName("horas_mensais");

                entity.Property(e => e.Inativo).HasColumnName("inativo");

                entity.Property(e => e.Matricula).HasColumnName("matricula");

                entity.Property(e => e.Nome)
                    .HasColumnName("nome")
                    .HasMaxLength(100);

                entity.Property(e => e.NomeCargo)
                    .HasColumnName("nome_cargo")
                    .HasMaxLength(50);

                entity.Property(e => e.NomeCentroCusto)
                    .HasColumnName("nome_centro_custo")
                    .HasMaxLength(50);

                entity.Property(e => e.NomeDivisao)
                    .HasColumnName("nome_divisao")
                    .HasMaxLength(50);

                entity.Property(e => e.NomeOrgao)
                    .HasColumnName("nome_orgao")
                    .HasMaxLength(50);

                entity.Property(e => e.NomeSetor)
                    .HasColumnName("nome_setor")
                    .HasMaxLength(50);

                entity.Property(e => e.NomeVinculo)
                    .HasColumnName("nome_vinculo")
                    .HasMaxLength(50);

                entity.Property(e => e.Padrao)
                    .HasColumnName("padrao")
                    .HasMaxLength(50);

                entity.Property(e => e.Pensionista).HasColumnName("pensionista");

                entity.Property(e => e.TempoServico).HasColumnName("tempo_servico");

                entity.Property(e => e.ValorDecimoTerceiro)
                    .HasColumnName("valor_decimo_terceiro")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorDeducoesObrigatorias)
                    .HasColumnName("valor_deducoes_obrigatorias")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorFerias)
                    .HasColumnName("valor_ferias")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorRemuneracao)
                    .HasColumnName("valor_remuneracao")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorRemuneracaoBasica)
                    .HasColumnName("valor_remuneracao_basica")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorVerbasEventuais)
                    .HasColumnName("valor_verbas_eventuais")
                    .HasColumnType("numeric(12,2)");

                entity.Property(e => e.ValorVerbasIndenizatorias)
                    .HasColumnName("valor_verbas_indenizatorias")
                    .HasColumnType("numeric(12,2)");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ToTable("usuario");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('usuario_seq'::regclass)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(100);

                entity.Property(e => e.Senha)
                    .IsRequired()
                    .HasColumnName("senha")
                    .HasMaxLength(200);
            });

            modelBuilder.HasSequence("convenio_seq");

            modelBuilder.HasSequence("empenho_seq");

            modelBuilder.HasSequence("licitacao_seq");

            modelBuilder.HasSequence("servidor_seq");

            modelBuilder.HasSequence("usuario_seq");
        }
    }
}
