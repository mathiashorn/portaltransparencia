﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalTransparencia.Models.Filter
{
    public class LicitacaoFilterModel : FilterModelBase
    {
        public string NrLicitacao { get; set; }
        public int? AnoLicitacao { get; set; }
        public string CdTipoModalidade { get; set; }
        public decimal? VlLicitacaoInicial { get; set; }
        public decimal? VlLicitacaoFinal { get; set; }

        public LicitacaoFilterModel() : base()
        {
            this.Limit = 3;
        }

        public override object Clone()
        {
            var jsonString = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject(jsonString, this.GetType());
        }
    }
}
