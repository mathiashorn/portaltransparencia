﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalTransparencia.Models.Filter
{
    public class ConvenioFilterModel : FilterModelBase
    {
        public DateTime? DataAssinaturaInicial { get; set; }
        public DateTime? DataAssinaturaFinal { get; set; }
        public string Convenio1 { get; set; }
        public string Convenente { get; set; }
        public decimal? ValorRecursoInicial { get; set; }
        public decimal? ValorRecursoFinal { get; set; }

        public ConvenioFilterModel() : base()
        {
            this.Limit = 3;
        }

        public override object Clone()
        {
            var jsonString = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject(jsonString, this.GetType());
        }
    }
}
