﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalTransparencia.Models.Filter
{
    public class ServidorFilterModel : FilterModelBase
    {
        public string Nome { get; set; }
        public string NomeCargo { get; set; }
        public decimal? ValorRemuneracaoInicial { get; set; }
        public decimal? ValorRemuneracaoFinal { get; set; }

        public ServidorFilterModel() : base()
        {
            this.Limit = 3;
        }

        public override object Clone()
        {
            var jsonString = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject(jsonString, this.GetType());
        }
    }
}
