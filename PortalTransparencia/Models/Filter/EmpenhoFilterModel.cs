﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalTransparencia.Models.Filter
{
    public class EmpenhoFilterModel : FilterModelBase
    {
        public DateTime? DataEmpenhoInicial { get; set; }
        public DateTime? DataEmpenhoFinal { get; set; }
        public string NumeroEmpenho { get; set; }
        public int? CodigoOrgao { get; set; }
        public decimal? ValorEmpenhoInicial { get; set; }
        public decimal? ValorEmpenhoFinal { get; set; }

        public EmpenhoFilterModel() : base()
        {
            this.Limit = 3;
        }

        public override object Clone()
        {
            var jsonString = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject(jsonString, this.GetType());
        }
    }
}
