﻿using System;
using System.Collections.Generic;

namespace PortalTransparencia.Models
{
    public partial class Servidor
    {
        public int Id { get; set; }
        public int? Matricula { get; set; }
        public string Nome { get; set; }
        public int? CodigoCargo { get; set; }
        public string NomeCargo { get; set; }
        public string Cpf { get; set; }
        public int? CodigoTipoMov { get; set; }
        public string DescTipoMov { get; set; }
        public int? HorasMensais { get; set; }
        public DateTime? DataAdmissao { get; set; }
        public int? CodigoSetor { get; set; }
        public string NomeSetor { get; set; }
        public int? CodigoDivisao { get; set; }
        public string NomeDivisao { get; set; }
        public int? CodigoOrgao { get; set; }
        public string NomeOrgao { get; set; }
        public int? CodigoCentroCusto { get; set; }
        public string NomeCentroCusto { get; set; }
        public int? CodigoVinculo { get; set; }
        public string NomeVinculo { get; set; }
        public string Padrao { get; set; }
        public DateTime? DataReferencia { get; set; }
        public char? Inativo { get; set; }
        public char? Pensionista { get; set; }
        public int? TempoServico { get; set; }
        public decimal? ValorRemuneracaoBasica { get; set; }
        public decimal? ValorVerbasEventuais { get; set; }
        public decimal? ValorVerbasIndenizatorias { get; set; }
        public decimal? ValorFerias { get; set; }
        public decimal? ValorDecimoTerceiro { get; set; }
        public decimal? ValorDeducoesObrigatorias { get; set; }
        public decimal? ValorRemuneracao { get; set; }
    }
}
