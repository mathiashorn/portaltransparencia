﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace PortalTransparencia.Models
{
    public partial class Orgao
    {
        public Orgao()
        {
            Empenho = new HashSet<Empenho>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }

        [JsonIgnore]
        public virtual ICollection<Empenho> Empenho { get; set; }
    }
}
