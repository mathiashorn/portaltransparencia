﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortalTransparencia.Models.Chart
{
    public class ChartObject
    {
        //public Option Options { get; set; }
        //public ICollection<Serie> Series { get; set; }

        public List<String> Options { get; set; }
        public List<decimal> Series { get; set; }

    }
}
