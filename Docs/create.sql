-- Table: public.orgao

-- DROP TABLE public.orgao;

CREATE TABLE public.orgao
(
  id integer NOT NULL,
  nome character varying(80) NOT NULL,
  CONSTRAINT orgao_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.orgao
  OWNER TO postgres; 
  
INSERT INTO public.orgao(id, nome) VALUES (02, 'GABINETE DO PREFEITO');
INSERT INTO public.orgao(id, nome) VALUES (03, 'SECRETARIA DE PLANEJAMENTO');
INSERT INTO public.orgao(id, nome) VALUES (04, 'ASSESSORIA JURIDICA');
INSERT INTO public.orgao(id, nome) VALUES (05, 'SECRETARIA DE ADMINISTRACAO');
INSERT INTO public.orgao(id, nome) VALUES (06, 'SECRETARIA DA FAZENDA');
INSERT INTO public.orgao(id, nome) VALUES (07, 'SECRETARIA DE OBRAS E SERVIÇOS URBANOS');
INSERT INTO public.orgao(id, nome) VALUES (08, 'SECRETARIA DE MEIO AMBIENTE');
INSERT INTO public.orgao(id, nome) VALUES (09, 'SECRETARIA DE AGRICULTURA E ABASTECIMENTO');
INSERT INTO public.orgao(id, nome) VALUES (10, 'SECRETARIA DE EDUCACAO');
INSERT INTO public.orgao(id, nome) VALUES (11, 'SECRET.TRABALHO, HABIT. E ASSIST. SOCIAL');
INSERT INTO public.orgao(id, nome) VALUES (12, 'SECRETARIA DE INDUSTRIA E COMERCIO');
INSERT INTO public.orgao(id, nome) VALUES (13, 'SECRETARIA DE CULTURA E TURISMO');
INSERT INTO public.orgao(id, nome) VALUES (14, 'SECRETARIA DE SAUDE');
INSERT INTO public.orgao(id, nome) VALUES (15, 'RESERVA DE CONTINGENCIA');
INSERT INTO public.orgao(id, nome) VALUES (16, 'SECRETARIA DA JUVENTUDE, ESPORTE E LAZER');
INSERT INTO public.orgao(id, nome) VALUES (17, 'SECRETARIA DE GOVERNO');
INSERT INTO public.orgao(id, nome) VALUES (18, 'SECRETARIA DE SEGURANÇA PÚBLICA E CIDADANIA');
INSERT INTO public.orgao(id, nome) VALUES (19, 'PROCURADORIA');
INSERT INTO public.orgao(id, nome) VALUES (20, 'FUNDO DE PREVIDÊNCIA SOCIAL DO MUNICÍPIO DE LAJEADO');


-- Table: public.empenho

-- DROP TABLE public.empenho;

CREATE SEQUENCE empenho_seq;

CREATE TABLE public.empenho
( 
  id integer NOT NULL DEFAULT nextval('empenho_seq'::regclass),
  orgao_id INT NOT NULL,
  codigo_unidade_orcamentaria character(2) NULL,
  codigo_funcao character(2) NULL,
  codigo_subfuncao character(3) NULL,
  codigo_programa character(4) NULL,
  codigo_projeto character(5) NULL,
  codigo_rubrica_despesa character varying(15) NULL,
  codigo_recurso_vinculado character(4) NULL,
  contrapartida_recurso character(4) NULL,
  numero_empenho character varying(13) NULL,
  data_empenho timestamp(0) without time zone NULL,
  valor_empenho decimal(12,2) NULL,
  sinal_valor character(1) NULL,
  codigo_credor character varying(10) NULL,
  caracteristica_peculiar character(3) NULL,
  registro_precos character(1) NULL,
  numero_licitacao character varying(20) NULL,
  ano_licitacao integer NULL,
  historico_empenho text NULL,
  modalidade_licitacao character(3) NULL,
  base_legal character(2) NULL,
  identificador_despesa_funcionario character(1) NULL,
  licitacao_compartilhada character(1) NULL,
  cnpj_gerenciador_licitacao character varying(14) NULL,  
  CONSTRAINT empenho_pkey PRIMARY KEY (id)
  CONSTRAINT fk_empenho_orgao,
    FOREIGN KEY (orgao_id)
    REFERENCES orgao (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.empenho
  OWNER TO postgres;
  
/*INSERT INTO public.empenho(
		codigo_orgao, codigo_unidade_orcamentaria, codigo_funcao, 
		codigo_subfuncao, codigo_programa, codigo_projeto, codigo_rubrica_despesa, 
		codigo_recurso_vinculado, contrapartida_recurso, numero_empenho, 
		data_empenho, valor_empenho, sinal_valor, codigo_credor, caracteristica_peculiar, 
		registro_precos, numero_licitacao, ano_licitacao, historico_empenho, 
		modalidade_licitacao, base_legal, identificador_despesa_funcionario, 
		licitacao_compartilhada, cnpj_gerenciador_licitacao)
VALUES ('07', '01', '04', 
		'122', '003', '02018', '339030010000000', 
		'0001', '0000', '0020181000001', 
		'2019-09-18 00:00:00', 112.40, '+', '0000004083', '000', 
		'N', '00000000000000000054', 2016, 'LICITA��O: 2016/54 (Preg�o Presencial) AQUISI��O DE COMBUST�VEL PARA O ABASTECIMENTO DE M�QUINAS PESADAS, CAMINH�ES E VE�CULOS LEVES. CONTRATO  159-01/2017 ORIDUNDO DO PROCESSO  2017/26932 E  ATA REGISTRO DE PRE�OS 101-04/2016, ATA COMPRAS  2016/39, PREG�O PRESENCIAL  054-06/2016 E PROCESSO LICITAT�RIO  29640/2016. VALIDADE: 30/11/2018. *REPONS�VEL: Cassiano Alberto Jung TELEFONE: (51) 3982-1078. ', 
		'PRP', '06', 'X', 
		NULL, NULL);*/

		
-- Table: public.licitacao

-- DROP TABLE public.licitacao;

CREATE SEQUENCE licitacao_seq;

CREATE TABLE public.licitacao
( 
  id integer NOT NULL DEFAULT nextval('licitacao_seq'::regclass),
  codigo_orgao character(5) NULL, 
  nr_licitacao character varying(20) NULL,
  ano_licitacao integer NULL,
  cd_tipo_modalidade character(3) NULL,	
  nr_comissao integer NULL,
  ano_comissao integer NULL,
  tp_comissao character(1) NULL, 
  nr_processo character varying(20) NULL,
  ano_processo integer NULL,
  tp_objeto character(3) NULL,
  cd_tipo_fase_atual character(3) NULL,
  tp_licitacao character(3) NULL,
  tp_nivel_julgamento character(1) NULL,
  dt_autorizacao_adesao timestamp(0) without time zone NULL,
  tp_caracteristica_objeto character(2) NULL,
  tp_natureza character(1) NULL,
  tp_regime_execucao character(1) NULL,
  bl_permite_subcontratacao character(1) NULL,
  tp_beneficio_micro_epp character(1) NULL,
  tp_fornecimento character(1) NULL,
  tp_atuacao_registro character(1) NULL,
  nr_licitacao_original character varying(20) NULL,
  ano_licitacao_original integer NULL,
  nr_ata_registro_preco character varying(20) NULL,
  dt_ata_registro_preco timestamp(0) without time zone NULL,
  pc_taxa_risco decimal(12,2) NULL,
  tp_execucao character(1) NULL,
  tp_disputa character(1) NULL,
  tp_prequalificacao character(1) NULL,
  bl_inversao_fases character(1) NULL,
  tp_resultado_global character(1) NULL,
  cnpj_orgao_gerenciador character varying(14) NULL,
  nm_orgao_gerenciador character varying(60) NULL,
  ds_objeto text NULL,
  cd_tipo_fundamentacao character varying(8) NULL,
  nr_artigo integer NULL,
  ds_inciso character varying(10) NULL,
  ds_lei character varying(10) NULL,
  dt_inicio_inscr_cred timestamp(0) without time zone NULL,
  dt_fim_inscr_cred timestamp(0) without time zone NULL,
  dt_inicio_vigen_cred timestamp(0) without time zone NULL,
  dt_fim_vigen_cred timestamp(0) without time zone NULL,
  vl_licitacao decimal(12,2) NULL,
  bl_orcamento_sigiloso character(1) NULL,
  bl_recebe_inscricao_per_vig character(1) NULL,
  bl_permite_consorcio character(1) NULL,
  dt_abertura timestamp(0) without time zone NULL,
  dt_homologacao timestamp(0) without time zone NULL,
  dt_adjudicacao timestamp(0) without time zone NULL,
  bl_licit_propria_orgao character(1) NULL,
  tp_documento_fornecedor character(1) NULL,
  nr_documento_fornecedor character varying(14) NULL,
  tp_documento_vencedor character(1) NULL,
  nr_documento_vencedor character varying(14) NULL,
  vl_homologado decimal(12,2) NULL,
  bl_gera_despesa character(1) NULL,
  ds_observacao text NULL,
  pc_tx_estimada decimal(12,2) NULL,
  pc_tx_homologada decimal(12,2) NULL,
  bl_compartilhada character(1) NULL,
  CONSTRAINT licitacao_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.licitacao
  OWNER TO postgres;
  
  
-- Table: public.usuario

-- DROP TABLE public.usuario;

CREATE SEQUENCE usuario_seq;

CREATE TABLE public.usuario
(
  id integer NOT NULL DEFAULT nextval('usuario_seq'::regclass),
  nome character varying(100) NOT NULL,
  senha character varying(200) NOT NULL,
  email character varying(100) NOT NULL,
  CONSTRAINT usuario_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.usuario
  OWNER TO postgres; 
  
INSERT INTO public.usuario(nome, senha, email) VALUES ('MATHIAS HORN', 'KOGnHXBha+a53ssT7aMZFb6CYcJVJkNPmWgRqkWURq8=', 'mathias.horn@gmail.com');


-- Table: public.convenio

-- DROP TABLE public.convenio;

CREATE SEQUENCE convenio_seq;

CREATE TABLE public.convenio
(
  id integer NOT NULL DEFAULT nextval('convenio_seq'::regclass),
  convenio character varying(11) NULL,
  administracao character varying(50) NULL,
  situacao character(1) NULL,
  convenente character varying(100) NULL,
  concedente character varying(100) NULL,
  interveniente character varying(100) NULL,
  tipo character varying(50) NULL,
  classificacao character varying(50) NULL,
  objeto text NULL,
  data_assinatura timestamp(0) without time zone NULL,
  data_encerramento timestamp(0) without time zone NULL,
  data_rescisao timestamp(0) without time zone NULL,
  data_inicio timestamp(0) without time zone NULL,
  data_vencimento timestamp(0) without time zone NULL,
  data_vencimento_original timestamp(0) without time zone NULL,
  recurso character varying(100) NULL,
  contrapartida character varying(50) NULL,
  valor_recurso decimal(12,2) NULL,
  valor_contrapartida decimal(12,2) NULL,
  valor_original decimal(12,2) NULL,
  valor_total decimal(12,2) NULL,
  CONSTRAINT convenio_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.convenio
  OWNER TO postgres;
  

-- Table: public.servidor

-- DROP TABLE public.servidor;

CREATE SEQUENCE servidor_seq;

CREATE TABLE public.servidor
(
  id integer NOT NULL DEFAULT nextval('servidor_seq'::regclass),
  matricula integer NULL,
  nome character varying(100) NULL,
  codigo_cargo integer NULL,
  nome_cargo character varying(50) NULL,
  cpf character varying(14) NULL,
  codigo_tipo_mov integer NULL,
  desc_tipo_mov character varying(50) NULL,
  horas_mensais integer NULL,
  data_admissao timestamp(0) without time zone,
  codigo_setor integer NULL,
  nome_setor character varying(50) NULL,
  codigo_divisao integer NULL,
  nome_divisao character varying(50) NULL,
  codigo_orgao INT NULL,
  nome_orgao character varying(50) NULL,
  codigo_centro_custo integer NULL,
  nome_centro_custo character varying(50) NULL,
  codigo_vinculo integer NULL,
  nome_vinculo character varying(50) NULL,
  padrao character varying(50) NULL,
  data_referencia timestamp(0) without time zone,
  inativo character(1) NULL,
  pensionista character(1) NULL,
  tempo_servico integer NULL,
  valor_remuneracao_basica decimal(12,2) NULL,
  valor_verbas_eventuais decimal(12,2) NULL,
  valor_verbas_indenizatorias decimal(12,2) NULL,
  valor_ferias decimal(12,2) NULL,
  valor_decimo_terceiro decimal(12,2) NULL,
  valor_deducoes_obrigatorias decimal(12,2) NULL,
  valor_remuneracao decimal(12,2) NULL,
  CONSTRAINT servidor_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.servidor
  OWNER TO postgres;  